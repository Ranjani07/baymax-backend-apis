<?php

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>

			    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			        <header class="entry-header">
                		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                	</header><!-- .entry-header -->
    				<div class="entry-content">
			            <?php the_content(); ?>
			        </div>
			    </article>

			<?php endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => 'Previous page',
				'next_text'          => 'Next page',
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . 'Page' . ' </span>',
			) );

		endif; ?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
