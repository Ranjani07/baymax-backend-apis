<?php

class APP_PHPEXCEL{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        include_once __DIR__ . '/classes/phpexcel/PHPExcel.php';
    }
    
    public static function generate($headers = array(), $data = array(), $filename = "report") {
        
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->setActiveSheetIndex(0);
        
        $column = 'A';
        $line = 1;
        
        foreach ($headers as $value) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                ->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->SetCellValue($column.$line, $value);
            $column = chr(ord($column) + 1);
        }
        
        $column = 'A';
        $line++;
        
        foreach ($data as $register) {
            $column = 'A';
            foreach ($register as $value) {
                if (isset($value[1]) && $value[1] === true) {
                    $objPHPExcel->getActiveSheet()
                        ->getStyle($column.$line)
                        ->getNumberFormat()
                        ->setFormatCode("#,##0.00");
                        $objPHPExcel->getActiveSheet()->SetCellValue($column.$line, $value[0]);
                } elseif (is_numeric($value[0])) {
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($column.$line, $value[0], PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue($column.$line, $value[0]);
                }
                $objPHPExcel->getActiveSheet()->getColumnDimension($column)
                ->setAutoSize(true);
                $column = chr(ord($column) + 1);
            }
            $line++;
        }
        
        $objPHPExcel->getActiveSheet()->setTitle('Report');
        
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        $objWriter->save('php://output');
    }
    
    
}
APP_PHPEXCEL::start();
