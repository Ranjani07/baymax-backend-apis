<?php
class Scolaa_THEME_V1{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
       // add_filter( 'login_errors',array(get_called_class(),'set_login_error_message'));
    } 
    
    public static function set_login_error_message($error){
        global $errors;
    	$err_codes = $errors->get_error_codes();
    
    	// Invalid username.
    	// Default: '<strong>ERROR</strong>: Invalid username. <a href="%s">Lost your password</a>?'
    	if ( in_array( 'invalid_username', $err_codes ) ||  in_array( 'incorrect_password', $err_codes )) {
    		$error = '<strong>ERROR</strong>: Invalid username or password.';
    	}
  
	    return $error;
    }
}
Scolaa_THEME_V1::start();
