<?php

Scolaa_Config::register_module(
    'theme',
    array(
        'version' => '1',
        'display_name' => 'scolaa Theme Module',
        'description' => 'some description',
        'enabled' => true,
        'order' => 10,
        'classes' => array(
            'class-key-1' => array(
                'filepath' => '/theme-class.php',
                'version' => '1.0.0',
                'display_name' => 'some name',
                'description' => 'Some description',
                'enabled' => true,
            ),
        ),
    )
);