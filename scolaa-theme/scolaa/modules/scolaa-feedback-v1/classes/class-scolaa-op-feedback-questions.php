<?php
class Scolaa_Op_Feedback_Questions{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
       
    } 
    
     public static function get_op_feedback_questions($query_args){
        global $wpdb;
        $table_name = 'op_feedback_questions';
        $pagenate_query = '';
        $order_by = '';
        $select_query = " ";
        $search_query='';
        $search_string = isset($query_args['search_string'])?$query_args['search_string'] : "";
        $current_page = isset($query_args['current_page'])? $query_args['current_page'] : 1;
        $record_per_page = isset($query_args['record_per_page'])? $query_args['record_per_page'] : 10;
        $sort_ope = isset($query_args['sort_ope'])?$query_args['sort_ope']:"";
        $sort_name = isset($query_args['sort_name'])?$query_args['sort_name']:"";
        
        if(isset($current_page) && !empty($current_page) && isset($record_per_page) && !empty($record_per_page) ){
            $pagenate_query = Scolaa_Api_Helper::build_pagenate_query($current_page,$record_per_page);    
        }
        $search_query = '(';
        if(isset($search_string) && !empty($search_string)){
            $fields = Scolaa_Hospitals_V1::get_op_feedback_questions_fields();
            
            foreach($fields as $key => $field){
                $search_query .= $field . " LIKE '%$search_string%' OR ";
            }
           
        }
       $search_query .= ' 1=1 )';
        
        unset($query_args['search_string']);
        unset($query_args['current_page']);
        unset($query_args['record_per_page']);
        unset($query_args['sort_ope']);
        unset($query_args['sort_name']);
        
       if(!empty($sort_ope) && !empty($sort_name)){          
            $order_by = "ORDER BY ".$sort_name." ".$sort_ope;
       }
       foreach($query_args as $key => $args){
                $select_query .= " $key = $args AND ";
          
       }  
       
       $select_query .= " enabled_disabled = 1 AND ";
       
        $total_count = $wpdb->get_results("SELECT COUNT(*) as total_count FROM $table_name WHERE $select_query $search_query $order_by $pagenate_query");
        $data['total_count'] = $total_count[0]->total_count;
        $query = "SELECT * FROM $table_name WHERE $select_query $search_query $order_by $pagenate_query";
        
        $queried_data = $wpdb->get_results($query);
       
        $data['queried_data'] = $queried_data;
        $data['queried_count'] = count($queried_data);
        $data['pagination'] = array(
                                'current_page'=>$current_page,
                                'record_per_page'=>$record_per_page
                            );
        return $data; 
        
    }  
    
    public static function insert_op_feedback_questions($op_feedback_questions_data){
        global $wpdb;
        $table_name = "op_feedback_questions"; 
        
        foreach($op_feedback_questions_data as $key => $value){
                $op_feedback_questions_data[$key] = sanitize_text_field($value);
        }
        $result = $wpdb->insert($table_name, $op_feedback_questions_data);
        if(!empty($wpdb->rows_affected)){
             if($op_feedback_questions_data['language_id'] == 1){
                    $insert_id = $wpdb->insert_id;
                    $update_query = "update $table_name set parent_id = $insert_id where op_feedback_question_id = $insert_id";
                    $update_results = $wpdb->get_results($update_query);
                }
            if(!empty($wpdb->rows_affected)){
                return array(
    				"status" => true,
    				"message" => "",
    				"data" => ["inserted_id"=>$wpdb->insert_id]
    			);
            }
        }
		return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
		);       
    }
    
    public static function update_op_feedback_questions($op_feedback_questions_data){
        global $wpdb;
        $table_name = "op_feedback_questions";
        $data = [];
        $where = [];
        foreach($op_feedback_questions_data as $key => $value){
            if($key == 'op_feedback_question_id'){
                $where[$key] = $value;
            }
            else{
                    $data[$key] = sanitize_text_field($value);
                }
        }
        $result = $wpdb->update( $table_name, $data, $where);
        if(!empty($wpdb->rows_affected)){
            return array(
				"status" => true,
				"message" => "",
				"data" => ["updated_id"=>$wpdb->insert_id]
			);
        }
		return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
		);      
    }
    
   
   
    public static function delete_op_feedback_questions($op_feedback_questions_data){
        global $wpdb;
        $table_name = 'op_feedback_questions';        
        $where = [];
        foreach($op_feedback_questions_data as $key => $value){
            if($key == 'op_feedback_question_id'){
                $where[$key] = $value;
            }
        }
        $update['enabled_disabled'] = 0;
        $result = $wpdb->update($table_name, $update, $where);
        
        if($result !== false && $result){
           $trash_data['table_name'] = $table_name;
            $trash_data['deleted_id'] = $op_feedback_questions_data['op_feedback_question_id'];
            $trash_data['deleted_by'] = '';
            $trash_data['reason_for_deletion'] = $op_feedback_questions_data['reason_for_deletion'];
            $trash_result = $wpdb -> insert('trash',$trash_data); 
            if(!empty($wpdb->rows_affected)){
                return array(
    				'status' => true,
    				'message' => 'deleted successfully',
    				'data' => array('deleted_id' => $wpdb->insert_id)
    			);
            }
        }
    
        return array(
			"status" => false,
			"message"=>$wpdb->last_error,
			'data' => []
	    );		
    }
    
    
    public static function get_available_op_feedback_questions($query_args){
        global $wpdb;
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $language_id = isset($query_args['language_id'])? $query_args['language_id'] : 0;
        if(!$hospital_id || !$language_id){
            return array(
                "status" => false,
                "data" => [$hospital_id,$language_id],
                "message" => 'Need correct details.. check your input data'
            );
        }
        /*$result = $wpdb->get_results("SELECT * 
                    FROM  `domains` t1
                    LEFT JOIN  `op_feedback_questions` t2 ON t2.parent_id = t1.id
                    WHERE t1.`hospital_id` = ".$hospital_id."
                    AND t1.`language_id` = ".$language_id);*/
                    
        
        $parent_query = "SELECT * FROM `op_domains`where `hospital_id` = $hospital_id 
        and `language_id` = $language_id and `enabled_disabled` = 1 ORDER BY  `order_number` ASC";
        $parent_query_result = $wpdb->get_results($parent_query);
        
        $feedback = [];            
        foreach($parent_query_result as $parent_question){
            $domain_id = isset($parent_question->op_domain_id)? $parent_question->op_domain_id : 0;
            $question_query = "SELECT * FROM `op_feedback_questions` t1
                LEFT OUTER JOIN `op_feedback_options` t2 on t1.op_feedback_question_id = t2.question_id
                where t1.`hospital_id` = ".$hospital_id."
                AND t1.`language_id` = ".$language_id." AND t1.`domain_id` = $domain_id";
                
            $question_result = $wpdb->get_results($question_query);
            $parent_question->child_questions = $question_result;
            $feedback['starting_questions'][]  = $parent_question;
                
        }
        
        $final_query = "SELECT * FROM `op_final_feedback_questions`where `hospital_id` = $hospital_id and `language_id` = $language_id and `enabled_disabled` = 1";
        $final_query_result = $wpdb->get_results($final_query);
        
        $feedback['final_questions'] = $final_query_result;
        $feedback = self::format_feedback_data($feedback);
        if(!empty($parent_query_result)){
            return array(
                "status" => true,
                "data" => $feedback,
                "message" => 'Available Feedback Questions data received successfully'
            );
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Questions Available'
        );
    }
    
    public static function format_feedback_data($feedback){
        $starting_questions = isset($feedback['starting_questions'])? $feedback['starting_questions'] : [];
        $final_questions = isset($feedback['final_questions'])? $feedback['final_questions'] : [];

        $parent_question = [];
        foreach($starting_questions as $key => $question){  
            $child_questions = $question->child_questions;

            $order_rearrange = [];
            foreach($child_questions as $child_question){ 
                $order_rearrange[$child_question->op_feedback_question_id]['feedback_question_id'] = $child_question->op_feedback_question_id;
                $order_rearrange[$child_question->op_feedback_question_id]['hospital_id'] = $child_question->hospital_id;
                $order_rearrange[$child_question->op_feedback_question_id]['domain_id'] = $child_question->domain_id;
                $order_rearrange[$child_question->op_feedback_question_id]['language_id'] = $child_question->language_id;
                $order_rearrange[$child_question->op_feedback_question_id]['parent_id'] = $child_question->parent_id;
                $order_rearrange[$child_question->op_feedback_question_id]['question_type'] = $child_question->question_type;
                $order_rearrange[$child_question->op_feedback_question_id]['question'] = $child_question->question;
                $order_rearrange[$child_question->op_feedback_question_id]['enabled_disabled'] = $child_question->enabled_disabled;
                $order_rearrange[$child_question->op_feedback_question_id]['comment_enabled_disabled'] = $child_question->comment_enabled_disabled;
                $order_rearrange[$child_question->op_feedback_question_id]['comment_head'] = $child_question->comment_head;
                $order_rearrange[$child_question->op_feedback_question_id]['created_by'] = $child_question->created_by;
                $order_rearrange[$child_question->op_feedback_question_id]['created_on'] = $child_question->created_on;
                $order_rearrange[$child_question->op_feedback_question_id]['question_order_number'] = $child_question->question_order_number;
                
                if(!empty($child_question->op_feedback_options_id)){
                    $order_rearrange[$child_question->op_feedback_question_id]['options'][] =  array(  
                        'feedback_options_id' => $child_question->op_feedback_options_id,
                        'options' => $child_question->options,
                        'options_order_number' => $child_question->options_order_number,
                        'parent_option_id' => $child_question->parent_option_id,
                        'comment_enabled_disabled' => $child_question->comment_enabled_disabled,
                        'comment_head' => $child_question->comment_head
                    );
                }
            
            }
            
            $question->child_questions = $order_rearrange;
            $starting_questions[$key] = $question;
        }
        return array(
            'starting_questions' => $starting_questions,
            'final_questions' => $final_questions
        );
        
    }
    
    
    public static function insert_op_feedback($op_feedback_data){
        global $wpdb;
        $wpdb->hide_errors();
        $table_name = "op_feedbacks";
        $patient_id = 0;
        $op_feedback_insert_request = [];
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();

        $patient_details = isset($op_feedback_data["patient_details"])? $op_feedback_data["patient_details"] : [];
        $starting_questions = isset($op_feedback_data["starting_questions"])? $op_feedback_data["starting_questions"] : [];
        $final_questions = isset($op_feedback_data["final_questions"])? $op_feedback_data["final_questions"] : [];
        
        /**
         * Need to check is empty field from request data
         */
        //patient validation
        $except = array('hospital_patient_id','patient_photo','patient_email','patient_gender');
        foreach($patient_details as $key => $value){
            if(!in_array($key,$except) && empty($patient_details[$key])){
                return array(
        				"status" => false,
        				"message" => 'Please fill all details',
        		        "data" => $patient_details
        		);    
            }
        }
        //starting question validation
        
        $flag = false;
        $except = array('rating','options','answer','comments');
        foreach($starting_questions as $question_key => $starting_question){
            foreach($starting_question as $key => $value){
                if(!in_array($key,$except) && empty($starting_question[$key])){
                    $flag = true;
                    goto starting_question_required;
                }
            }
        }
        
starting_question_required: 
        if($flag){
            return array(
    				"status" => false,
    				"message" => 'Please fill all details',
    		        "data" => $starting_questions
    		);    
        }
        
        //final question validation
         
        $flag = false;
        $except = array('rating');
        foreach($final_questions as $question_key => $final_question){
            foreach($final_question as $key => $value){
                if(!in_array($key,$except) && empty($final_question[$key])){
                    $flag = true;
                    goto final_question_required;
                }
            }
        }
        
final_question_required: 
        if($flag){
            return array(
    				"status" => false,
    				"message" => 'Please fill all details',
    		        "data" => $final_questions
    		);    
        } 
        
        
        /**
         * Patient Details Insertion
         */ 
        
        $hospital_patient_id = !empty($patient_details['hospital_patient_id'])? $patient_details['hospital_patient_id'] : false;


        $patient_details_insert_data = [];
        $file_name = time();
        $patient_table = 'patient_details';
        $patient_details_insert_data['hospital_id'] = $hospital_id;
        
        /**
         * get photo from MD array
         */
         /*
        $photo_details = isset($_FILES['feedback_details'])? $_FILES['feedback_details'] : [];
        $photo_file_array = [];
        foreach($photo_details as $key => $value){
            $photo_file_array[$key] = isset($value['patient_details']['patient_photo'])? $value['patient_details']['patient_photo'] : '';
        }
        
        $patient_details_insert_data['patient_photo'] = isset($photo_file_array)? $photo_file_array : array();
        if(isset($patient_details_insert_data['patient_photo']) && !empty($patient_details_insert_data['patient_photo'])){
            $patient_details_insert_data['patient_photo'] = $wpdb->_real_escape(self::upload_picture($patient_details_insert_data['patient_photo'],$file_name));
        }*/
        
        
        Scolaa_Feedback_V1::file_log([$patient_details],"insert-feedback");
        if(isset($patient_details['patient_photo']) && !empty($patient_details['patient_photo'])){
            $upload_dir = wp_upload_dir(); 
            $path = $upload_dir['basedir']."/patients/";
            $url = $upload_dir['baseurl'].'/patients/';
 
            $img = $patient_details['patient_photo'];
            $image_parts = explode(";base64,", $img);
            $image_base64 = base64_decode($image_parts[1],true);
            
            if(!$image_base64){
                $patient_details_insert_data['patient_photo'] = '';
                
            }else{
                $filename = uniqid() . '.png';
                $file = $path . $filename;
                if(!file_exists($file)){
                    Scolaa_Api_Helper::init_media_folder($path);
                }
                file_put_contents($file, $image_base64);
                Scolaa_Feedback_V1::file_log([$file,$image_parts[1]],"insert-feedback");
                $patient_details_insert_data['patient_photo'] = $wpdb->_real_escape($url. $filename);
            }
        }
       
        
        
        $patient_details_insert_data['hospital_patient_id'] = $hospital_patient_id;
        $patient_details_insert_data['patient_name'] = isset($patient_details['patient_name'])? $patient_details['patient_name'] : '';
    	$patient_details_insert_data['patient_mobile_number'] = isset($patient_details['mobile_number'])? $patient_details['mobile_number'] : '';
    	$patient_details_insert_data['patient_email'] = isset($patient_details['email'])? $patient_details['email'] : '';
		$patient_details_insert_data['patient_gender'] = isset($patient_details['patient_gender'])? $patient_details['patient_gender'] : '';
		$patient_details_insert_data['language_id_used'] = isset($patient_details['language_id_used'])? $patient_details['language_id_used'] : '';
		$patient_details_insert_data['mr_number'] = isset($patient_details['mr_number'])? $patient_details['mr_number'] : '';
		$patient_details_insert_data['room_number'] = isset($patient_details['room_number'])? $patient_details['room_number'] : '';
	    $patient_details_insert_data['created_by'] = get_current_user_id();
	    
	    foreach($patient_details_insert_data as $key => $value){
                $patient_details_insert_data[$key] = sanitize_text_field($value);
        }
        
	    $result['patients_details'] = $wpdb->insert($patient_table, $patient_details_insert_data);
	    if(empty($wpdb->rows_affected)){
        	return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
    		);   
        }
        $patient_id = $wpdb->insert_id;
        if(empty($patient_id)){
            return array(
				"status" => false,
				"message" => 'Error: Please Contact Baymax Team',
		        "data" => [$patient_details]
    		);   
        }
        
        
        /**
         * Starting Question Insertion
         */
        $starting_questions_insert_data = [];
        $starting_questions_insert_data['patient_id'] = $patient_id;
        $starting_questions_insert_data['hospital_id'] = $hospital_id;
        $starting_questions_table = 'op_feedbacks';
        
        foreach($starting_questions as $question_key => $starting_question){
            foreach($starting_question as $key => $value){
                $starting_questions_insert_data['parent_id'] = isset($starting_question['parent_id'])?$starting_question['parent_id'] : ''; 
                $starting_questions_insert_data['department_id'] = isset($starting_question['department_id'])?$starting_question['department_id'] : ''; 
                $starting_questions_insert_data['question_type'] = isset($starting_question['question_type'])?$starting_question['question_type'] : ''; 
                $starting_questions_insert_data['rating'] = isset($starting_question['rating'])?$starting_question['rating'] : '';
                $starting_questions_insert_data['options'] = isset($starting_question['options'])?$starting_question['options'] : '';
                $starting_questions_insert_data['answer'] = isset($starting_question['answer'])?$starting_question['answer'] : '';
                $starting_questions_insert_data['comments'] = isset($starting_question['comments'])?$starting_question['comments'] : '';
                $starting_questions_insert_data['device_id'] = isset($starting_question['device_id'])?$starting_question['device_id'] : '';
            }
            foreach($starting_questions_insert_data as $key => $value){
                $starting_questions_insert_data[$key] = sanitize_text_field($value);
            }
    	    $result['starting_questions'][] = array(
    	        "status" => $wpdb->insert($starting_questions_table, $starting_questions_insert_data),
    	        "message" => $wpdb->last_error
            );
        }
        
        /**
         * Final Questions Insertion
         * 
         */
        
        $final_questions_insert_data = [];
        $final_questions_insert_data['patient_id'] = $patient_id;
        $final_questions_insert_data['hospital_id'] = $hospital_id;
        $final_questions_table = 'op_final_feedbacks';
        
        foreach($final_questions as $question_key => $final_question){
            foreach($final_question as $key => $value){
                $final_questions_insert_data['parent_id'] = isset($final_question['parent_id'])?$final_question['parent_id'] : '';
                $final_questions_insert_data['department_id'] = isset($final_question['department_id'])?$final_question['department_id'] : '';
                $final_questions_insert_data['question_type'] = isset($final_question['question_type'])?$final_question['question_type'] : ''; 
                $final_questions_insert_data['rating'] = isset($final_question['rating'])?$final_question['rating'] : '';
                $final_questions_insert_data['device_id'] = isset($final_question['device_id'])?$final_question['device_id'] : '';
            }
            foreach($final_questions_insert_data as $key => $value){
                $final_questions_insert_data[$key] = sanitize_text_field($value);
            }
            
            
             /**Count increment for  NPS score calculation
         * @created_by Ranjani
        */
            
            
            if($final_questions_insert_data['question_type'] == "nps"){
                if($final_questions_insert_data['rating']>=0 && $final_questions_insert_data['rating'] <=6){
                    $nps_update_query = "update net_promoter_score set detractors = detractors + 1 , total_patients = total_patients + 1 where hospital_id = $hospital_id";
                }
                else if($final_questions_insert_data['rating'] == 7 || $final_questions_insert_data['rating'] == 8){
                    $nps_update_query = "update net_promoter_score set passives = passives + 1 , total_patients = total_patients + 1 where hospital_id = $hospital_id";
                }
                else{
                    $nps_update_query = "update net_promoter_score set promoters = promoters + 1,total_patients = total_patients + 1 where hospital_id = $hospital_id";
                }
                $nps_result = $wpdb->get_results($nps_update_query);
                $log_nps['patient_id'] = $patient_id;
                $log_nps['query'] = $nps_update_query;
                Scolaa_Feedback_V1::file_log($log_nps,"nps-op");
            }
            
            
            
            
            
    	    $result['final_questions'][] =  array(
                "insert_status" => $wpdb->insert($final_questions_table, $final_questions_insert_data),
                "message" => $wpdb->last_error
            );
        }
        Scolaa_Feedback_V1::file_log($result,"request-response");
        return array(
    		"status" => true,
    		"message" => 'Feedback inserted successfully',
    		"data" => $result
        );         
    }
    
    
    public static function upload_picture($picture,$file_name){
        $upload_dir = wp_upload_dir();
        $file_path = $upload_dir['basedir'];
        $picture['name'] = $file_name.'.png';
        $center_path = $file_path. '/patients';
        $file_full_path = $file_path. '/patients/'.$file_name.'.png';
        if(!file_exists($file_full_path)){
            Scolaa_Api_Helper::init_media_folder($center_path);
            copy( $picture['tmp_name'], $center_path.'/'.$picture['name'] );
            return $upload_dir['baseurl'] . '/patients/' .$picture['name'];
        }
        return '';
    }
    
    
}
Scolaa_Op_Feedback_Questions::start();