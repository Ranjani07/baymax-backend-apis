<?php
class Scolaa_Domains{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
       
    } 
    
     public static function get_domains($query_args){
        global $wpdb;
        $table_name = 'domains';
        $pagenate_query = '';
        $order_by = '';
        $select_query = " ";
        $search_query='';
        $search_string = isset($query_args['search_string'])?$query_args['search_string'] : "";
        $current_page = isset($query_args['current_page'])? $query_args['current_page'] : 1;
        $record_per_page = isset($query_args['record_per_page'])? $query_args['record_per_page'] : 10;
        $sort_ope = isset($query_args['sort_ope'])?$query_args['sort_ope']:"";
        $sort_name = isset($query_args['sort_name'])?$query_args['sort_name']:"";
        
        if(isset($current_page) && !empty($current_page) && isset($record_per_page) && !empty($record_per_page) ){
            $pagenate_query = Scolaa_Api_Helper::build_pagenate_query($current_page,$record_per_page);    
        }
        $search_query = '(';
        if(isset($search_string) && !empty($search_string)){
            $fields = Scolaa_Hospitals_V1::get_domains_fields();
            
            foreach($fields as $key => $field){
                $search_query .= $field . " LIKE '%$search_string%' OR ";
            }
           
        }
       $search_query .= ' 1=1 )';
        
        unset($query_args['search_string']);
        unset($query_args['current_page']);
        unset($query_args['record_per_page']);
        unset($query_args['sort_ope']);
        unset($query_args['sort_name']);
        
       if(!empty($sort_ope) && !empty($sort_name)){          
            $order_by = "ORDER BY ".$sort_name." ".$sort_ope;
       }
       foreach($query_args as $key => $args){
                $select_query .= " $key = $args AND ";
          
       }  
       
       $select_query .= " enabled_disabled = 1 AND ";
       
        $total_count = $wpdb->get_results("SELECT COUNT(*) as total_count FROM $table_name WHERE $select_query $search_query $order_by $pagenate_query");
        $data['total_count'] = $total_count[0]->total_count;
        $query = "SELECT * FROM $table_name WHERE $select_query $search_query $order_by $pagenate_query";
        
        $queried_data = $wpdb->get_results($query);
       
        $data['queried_data'] = $queried_data;
        $data['quired_count'] = count($queried_data);
        $data['pagination'] = array(
                                'current_page'=>$current_page,
                                'record_per_page'=>$record_per_page
                            );
        return $data; 
        
    }  
    
    public static function insert_domains($domains_data){
        global $wpdb;
        $table_name = "domains"; 
        
        foreach($domains_data as $key => $value){
                $domains_data[$key] = sanitize_text_field($value);
        }
        $result = $wpdb->insert($table_name, $domains_data);
        if(!empty($wpdb->rows_affected)){
            return array(
				"status" => true,
				"message" => "",
				"data" => ["inserted_id"=>$wpdb->insert_id]
			);
        }
		return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
		);       
    }
    
    public static function update_domains($domains_data){
        global $wpdb;
        $table_name = "domains";
        $data = [];
        $where = [];
        foreach($domains_data as $key => $value){
            if($key == 'id'){
                $where[$key] = $value;
            }
            else {
                    $data[$key] = sanitize_text_field($value);
                }
        }
        
        $result = $wpdb->update( $table_name, $data, $where);
        if(!empty($wpdb->rows_affected)){
            return array(
				"status" => true,
				"message" => "",
				"data" => ["updated_id"=>$wpdb->insert_id]
			);
        }
		return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
		);      
    }
    
   
   
    public static function delete_domains($domains_data){
        global $wpdb;
        $table_name = 'domains';        
        $where = [];
        foreach($domains_data as $key => $value){
            if($key == 'id'){
                $where[$key] = $value;
            }
        }
        $update['enabled_disabled'] = 0;
        $result = $wpdb->update($table_name, $update, $where);
        
        $trash_data['table_name'] = $table_name;
        $trash_data['deleted_id'] = $domains_data['id'];
        $trash_data['deleted_by'] = '';
        $trash_data['reason_for_deletion'] = $domains_data['reason_for_deletion'];
        $trash_result = $wpdb -> insert('trash',$trash_data);
        
        if(!empty($wpdb->rows_affected)){
            return array(
				"status" => true,
				"inserted_id"=>$wpdb->insert_id
			);
        }
		return array(
				"status" => false,
				"db_error"=>$wpdb->last_error
		);
    }
    
    
    public static function get_available_op_areas(){
        global $wpdb;
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        if(!$hospital_id){
            return array(
                "status" => false,
                "data" => $hospital_id,
                "message" => 'Need correct details.. check your input data'
            );
        }
        $areas_query = "select op_areas_id,areas from op_areas where hospital_id = $hospital_id and enabled_disabled = 1";
        $areas_result = $wpdb->get_results($areas_query);
        if(!empty($areas_result)){
            return array(
                "status" => true,
                "data" => $areas_result,
                "message" => 'Available Areas data received successfully'
            );
        }
    }
    
    public static function insert_op_reviews($op_reviews_data){
        global $wpdb;
        $table_name = "op_reviews"; 
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        if(!$hospital_id){
            return array(
                "status" => false,
                "data" => $hospital_id,
                "message" => 'Need correct details.. check your input data'
            );
        }
        $op_reviews_data['hospital_id'] = $hospital_id;
        foreach($op_reviews_data as $key => $value){
                $op_reviews_data[$key] = sanitize_text_field($value);
        }
        $result = $wpdb->insert($table_name, $op_reviews_data);
        if(!empty($wpdb->rows_affected)){
            return array(
				"status" => true,
				"data" => ["inserted_id"=>$wpdb->insert_id],
				"message" => ""
			);
        }
		return array(
				"status" => false,
		        "data" => [],
		        "message" => $wpdb->last_error,
		);       
    }
   
    
}
Scolaa_Domains::start();