<?php
class Scolaa_Ip_Dashboard{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
       
    }
     
    public static function get_total_feedbacks($query_args)
    {
        global $wpdb;
        $total_feedbacks = 0;
        $table_name = "patient_details";
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $query = "SELECT COUNT( * ) as total_count FROM  $table_name WHERE hospital_id =$hospital_id";
        $result = $wpdb->get_results($query);
        if(!empty($result[0]->total_count)){
            $total_feedbacks = $result[0]->total_count;
        }
        return $total_feedbacks;
    }
    
    public static function get_average_overall_rating($query_args)
    {
        global $wpdb;
        $average_overall_rating = 0;
        $table_name = "ip_final_feedbacks";
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $query = "SELECT AVG(rating) as average FROM  $table_name WHERE hospital_id =$hospital_id and question_type = '"."overall_rating"."'";
        $result = $wpdb->get_results($query);
        if(!empty($result[0]->average)){
            $average_overall_rating = $result[0]->average;
        }
        return $average_overall_rating;
    }
    
    public static function get_average_nps($query_args)
    {
        global $wpdb;
        $average_nps = 0;
        $table_name = "ip_final_feedbacks";
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $query = "SELECT AVG(rating) as average FROM  $table_name WHERE hospital_id =$hospital_id and question_type = '"."nps"."'";
        $result = $wpdb->get_results($query);
        if(!empty($result[0]->average)){
            $average_nps = $result[0]->average;
        }
        return $average_nps;
    }
    
    public static function get_net_promoter_score($query_args)
    {
        global $wpdb;
        $table_name = "net_promoter_score";
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $query = "SELECT * FROM net_promoter_score where hospital_id = $hospital_id";
        $result = $wpdb->get_results($query);
        if(!empty($result[0])){
            $promoters_percentage = (($result[0]->promoters)/($result[0]->total_patients))*100;
            $detractors_percentage = (($result[0]->detractors)/($result[0]->total_patients))*100;
            $nps = $promoters_percentage - $detractors_percentage;
        }
        return $nps;
    }
    
    public static function get_nps_graph($query_args)
    {
        global $wpdb;
        $response = array();
        $table_name = "ip_final_feedbacks";
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
         if(empty($query_args['start_date'])){
            $query_args['start_date'] = date('Y-m-d H:i:s',strtotime("-30 days"));
        }
        if(empty($query_args['end_date'])){
            $query_args['end_date'] = date('Y-m-d H:i:s');
        }
        $start_date = new DateTime($query_args['start_date']); 
        $end_date = new DateTime( $query_args['end_date']);
        for($current_date = $start_date; $current_date <= $end_date; $current_date->modify('+1 day')){
            $current_date_string = $current_date->format("Y-m-d");
            $query = "SELECT AVG(rating) as average FROM  $table_name WHERE hospital_id =$hospital_id AND DATE(date) = '".$current_date_string."' AND question_type = '"."nps"."'";
            $result = $wpdb->get_results($query);
            // if(!empty($result[0]->date))
            //     $response[$result[0]->date] = $result[0]->average;
            if(!empty($result[0]->average)){
                 $response['label'][] = $current_date_string;
                 $response['data'][] = $result[0]->average;
            }
             else{
                $response['label'][] = $current_date_string;
                 $response['data'][] = 0;
             }
            $response['backgroundColor'][] = '#cdaaf9';
        }
        if(!empty($response)){
            return array(
                "status" => true,
                "data" => $response,
                "message" => 'NPS graph data received successfully'
            );
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No NPS Available'
        );
    }
    
    public static function get_overall_rating_graph($query_args)
    {
        global $wpdb;
        $response = array();
        $table_name = "ip_final_feedbacks";
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        if(empty($query_args['start_date'])){
            $query_args['start_date'] = date('Y-m-d H:i:s',strtotime("-30 days"));
        }
        if(empty($query_args['end_date'])){
            $query_args['end_date'] = date('Y-m-d H:i:s');
        }
        $start_date = new DateTime($query_args['start_date']);
        $start_date_string = $start_date->format('Y-m-d 00:00:00'); 
        $end_date = new DateTime( $query_args['end_date']);
        $end_date_string = $end_date->format('Y-m-d 23:23:59');
        
        for($rating = 1;$rating <= 5; $rating++)
        {
            $key_data = '';
            $query = "SELECT count(*) as count FROM  $table_name WHERE hospital_id =$hospital_id AND question_type = '"."overall_rating"."' AND rating = $rating AND date BETWEEN '".$start_date_string."' AND '".$end_date_string."'";
            $result = $wpdb->get_results($query);
            if($rating == 1){
                $key_data = 'Poor';
            } else if($rating == 2){
                $key_data = 'Fair';
            } else if($rating == 3){
                $key_data = 'Average';
            } else if($rating == 4){
                $key_data = 'Good';
            } else if($rating == 5){
                $key_data = 'Excellent';
            }
            if(!empty($result[0]->count)){
                $response['label'][] = $key_data;
                $response['data'][] = $result[0]->count;
            }
             else{
                $response['label'][] = $key_data;
                $response['data'][] = 0;
             }
            
        }
        if(!empty($response)){
            return array(
                "status" => true,
                "data" => $response,
                "message" => 'Overall Rating graph data received successfully'
            );
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Overall rating Available'
        );
    }
    
    public static function get_profile($query_args)
    {
        global $wpdb;
        $wordpress_user_id = get_current_user_id();
        $query = "select U.*,H.hospital_logo,H.hospital_name from user_details U JOIN hospitals H ON U.hospital_id = H.id where wordpress_user_id = $wordpress_user_id";
        $result = $wpdb -> get_results($query);
        if(!empty($result[0])){
            $plan_id = $result[0]->current_plan_id;
            //$plan_query = "select table_key from plan_details P JOIN hospitals H ON P.plan_id=H.current_plan_id where H.id=$hospital_id";
            $plan_query = "select table_key from plan_details where plan_id = $plan_id";
            $plan_result = $wpdb->get_results($plan_query);
            if(!empty($plan_result[0])){
                $profile = array();
                $profile['user_details'] = $result[0];
                $profile['user_data'] = get_userdata($wordpress_user_id);
                $profile['current_plan'] = $plan_result[0]->table_key;
                return array(
                    "status" => true,
                    "data" => $profile,
                    "message" => 'Profile data received successfully'
                );
            }
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Profile Available'
        );
    }
    
    public static function get_dashboard_details($query_args)
    {
        return array(
            'total_feedback' => self::get_total_feedbacks($query_args),
            'overall_rating' => self::get_average_overall_rating($query_args),
            'nps' => self::get_net_promoter_score($query_args)
        );
    }
    
    public static function get_suggestions($query_args)
    {
        global $wpdb;
        $response = array();
        $table_name = "ip_feedbacks";
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $pagenate_query = '';
        $order_by = '';
        $select_query = " ";
        $search_query='';
        $search_string = isset($query_args['search_string'])?$query_args['search_string'] : "";
        $current_page = isset($query_args['current_page'])? $query_args['current_page'] : 1;
        $record_per_page = isset($query_args['record_per_page'])? $query_args['record_per_page'] : 10;
        $sort_ope = isset($query_args['sort_ope'])?$query_args['sort_ope']:"";
        $sort_name = isset($query_args['sort_name'])?$query_args['sort_name']:"";
        $start_date = isset($query_args['start_date'])?$query_args['start_date']:"";
        $end_date = isset($query_args['end_date'])?$query_args['end_date']:"";
        $between = '';
        
        /**
         *Default for 30days if not selected by user
         * @edited by pradeep
         */
        if(empty($query_args['start_date'])){
            $query_args['start_date'] = date('Y-m-d',strtotime("-30 days"));
        }
        if(empty($query_args['end_date'])){
            $query_args['end_date'] = date('Y-m-d');
        }
        
        if(isset($current_page) && !empty($current_page) && isset($record_per_page) && !empty($record_per_page) ){
            $pagenate_query = Scolaa_Api_Helper::build_pagenate_query($current_page,$record_per_page);    
        }
        
        if(isset($search_string) && !empty($search_string)){
            $search_query = 'AND (';
            $fields = Scolaa_Feedback_V1::get_dashboard_suggestion_fields();
            
            foreach($fields as $key => $field){
                $search_query .= $field . " LIKE '%$search_string%' OR ";
            }
            $search_query = substr($search_query,0,-3).")";
        }
     

        if(isset($start_date) && isset($end_date) && !empty($start_date) && !empty($end_date)){
            $between = "AND F.date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:23:59'";
        }

        unset($query_args['search_string']);
        unset($query_args['current_page']);
        unset($query_args['record_per_page']);
        unset($query_args['sort_ope']);
        unset($query_args['sort_name']);
        unset($query_args['start_date']);
        unset($query_args['end_date']);
        
      if(!empty($sort_ope) && !empty($sort_name)){          
            $order_by = "ORDER BY ".$sort_name." ".$sort_ope;
      }
      else{
          $order_by = "ORDER BY F.date DESC";
      }
      if(!empty($query_args)){
          foreach($query_args as $key => $args){
              if($key == 'date'){
                  $select_query .= "AND F.date BETWEEN '".$args." 00:00:00' AND '".$args." 23:23:59'";
              }
              else if($key == 'rating'){
                  $select_query .= "AND O.rating = '".$args."'";
              }
              else if($key == 'patient_id'){
                  $select_query .= "AND F.patient_id = '".$args."'";
              }
              else{
                   $select_query .= "AND $key = '".$args."'";
              }
          }  
      }
     
      $total_count = $wpdb->get_results("SELECT count(*) as total_count
                    FROM ip_feedbacks F 
                        JOIN patient_details P ON F.patient_id = P.patient_id
                        JOIN ip_final_feedbacks O ON F.patient_id = O.patient_id
                    WHERE F.question_type = 'suggestion' AND F.hospital_id = $hospital_id AND O.question_type='nps' $select_query $search_query $between");
      $data['total_count'] = $total_count[0]->total_count;
      
      $query = "SELECT F.patient_id,F.answer,F.date,F.parent_id,P.*,O.rating,O.question_type
                    FROM ip_feedbacks F 
                        JOIN patient_details P ON F.patient_id = P.patient_id
                        JOIN ip_final_feedbacks O ON F.patient_id = O.patient_id
                    WHERE F.question_type = 'suggestion' AND F.hospital_id = $hospital_id AND O.question_type='nps' $select_query $search_query $between $order_by $pagenate_query";
     //print_r($query);
      $result = $wpdb->get_results($query);
     
      if(!empty($result)){
            foreach($result as $record)
            {
                $patient_id = $record->patient_id;
                $response['suggestions'][$patient_id]['patient_id'] = $patient_id;
                $response['suggestions'][$patient_id]['patient_name'] = $record->patient_name;
                $response['suggestions'][$patient_id]['respondent'] = $record->respondent;
                $response['suggestions'][$patient_id]['mr_number'] = $record->mr_number;
                $response['suggestions'][$patient_id]['room_number'] = $record->room_number;
                $response['suggestions'][$patient_id]['patient_gender'] = $record->patient_gender;
                if($record->patient_mobile_number!=''){
                    $response['suggestions'][$patient_id]['patient_mobile_number'] = substr_replace($record->patient_mobile_number,"*******",0,-3);
                }
                else{
                    $response['suggestions'][$patient_id]['patient_mobile_number'] = '';
                }
                $response['suggestions'][$patient_id]['answer'] = $record->answer;
                $response['suggestions'][$patient_id]['date'] = $record->date;
                $response['suggestions'][$patient_id]['NPS'] = $record->rating;
            }
            $response['suggestions'] = array_values($response['suggestions']);
            $parent_id = $result[0]->parent_id;
            $question_query = "select question from ip_feedback_questions where ip_feedback_question_id=$parent_id";
            $question_result = $wpdb -> get_results($question_query);
            
            if(!empty($question_result)){
                $response['question'] = $question_result[0]->question;
                $data['queried_data'] = $response;
                $data['queried_count'] = count($result);
                $data['pagination'] = array(
                                        'current_page'=>$current_page,
                                        'record_per_page'=>$record_per_page
                                    );
                    }
            return array(
                "status" => true,
                "data" => $data,
                "message" => 'Suggestions received successfully'
            );
        }
        
         else{
            return array(
            "status" => false,
            "data" => [],
            "message" => 'No suggestions available'
            );
        }
       
    }
    
    /*
    
    
    //The following functions contains codes for getting segments without language partition
    
    
    public static function get_segments($query_args)
    {
        global $wpdb;
        $response = array();
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        
        $parent_query = "SELECT * FROM `ip_domains`where `hospital_id` = $hospital_id 
        and `language_id` = 1 and `enabled_disabled` = 1 ORDER BY  `order_number` ASC";
        $parent_query_result = $wpdb->get_results($parent_query);
        
        $feedback = [];            
        foreach($parent_query_result as $parent_question){
            $domain_id = isset($parent_question->ip_domain_id)? $parent_question->ip_domain_id : 0;
            $question_query = "SELECT * FROM `ip_feedback_questions` t1
                LEFT OUTER JOIN `ip_feedback_options` t2 on t1.ip_feedback_question_id = t2.question_id
                where t1.`hospital_id` = ".$hospital_id."
                AND t1.`language_id` = 1 AND t1.`domain_id` = $domain_id";
                
            $question_result = $wpdb->get_results($question_query);
            $parent_question->child_questions = $question_result;
            $feedback['starting_questions'][]  = $parent_question;
                
        }
        $feedback = self::format_feedback_data($feedback);
        if(!empty($parent_query_result)){
            return array(
                "status" => true,
                "data" => $feedback,
                "message" => 'Available Feedback Questions data received successfully'
            );
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Questions Available'
        );
    }
    
    public static function format_feedback_data($feedback){
        global $wpdb;
        $starting_questions = isset($feedback['starting_questions'])? $feedback['starting_questions'] : [];
        $final_questions = isset($feedback['final_questions'])? $feedback['final_questions'] : [];
    
        $available_ratings = [1,2,3,4,5];
        $temp_rating = [];
        $parent_question = [];
        foreach($starting_questions as $key => $question){  
            $child_questions = $question->child_questions;

            $order_rearrange = [];
            foreach($child_questions as $child_question){ 
                $order_rearrange[$child_question->ip_feedback_question_id]['feedback_question_id'] = $child_question->ip_feedback_question_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['hospital_id'] = $child_question->hospital_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['domain_id'] = $child_question->domain_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['language_id'] = $child_question->language_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['parent_id'] = $child_question->parent_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['question_type'] = $child_question->question_type;
                $order_rearrange[$child_question->ip_feedback_question_id]['question'] = $child_question->question;
                $order_rearrange[$child_question->ip_feedback_question_id]['enabled_disabled'] = $child_question->enabled_disabled;
                $order_rearrange[$child_question->ip_feedback_question_id]['comment_enabled_disabled'] = $child_question->comment_enabled_disabled;
                $order_rearrange[$child_question->ip_feedback_question_id]['comment_head'] = $child_question->comment_head;
                $order_rearrange[$child_question->ip_feedback_question_id]['created_by'] = $child_question->created_by;
                $order_rearrange[$child_question->ip_feedback_question_id]['created_on'] = $child_question->created_on;
                $order_rearrange[$child_question->ip_feedback_question_id]['question_order_number'] = $child_question->question_order_number;
                
                $parent_id = $child_question->parent_id;
                if($child_question->question_type == 'star_rating')
                {
                    $star_rating_query = "SELECT  `parent_id` ,  `rating` , COUNT(  `rating` ) AS count
                                            FROM  `ip_feedbacks` 
                                            WHERE question_type =  'star_rating' AND parent_id = $parent_id
                                            GROUP BY parent_id, rating";
                    $star_rating_result = $wpdb -> get_results($star_rating_query,ARRAY_A);
                    
                    foreach($star_rating_result as $data){
                        foreach($available_ratings as $rating){
                            if($data['rating'] == $rating){
                                $temp_rating[$child_question->ip_feedback_question_id]['answer_count'][$data['rating']] = $data['count'];
                            }else{
                                if(
                                    empty($temp_rating[$child_question->ip_feedback_question_id]['answer_count'][$rating]) 
                                ){
                                    $temp_rating[$child_question->ip_feedback_question_id]['answer_count'][$rating] = 0;
                                }
                            }
                        }
                        
                    }
                    
                   // $count_array[$rating] = $star_rating_result[0]->count;
                    
                    $order_rearrange[$child_question->ip_feedback_question_id]['answer_count'] = $temp_rating[$child_question->ip_feedback_question_id]['answer_count'];
                }
                
                if(!empty($child_question->ip_feedback_options_id)){
                    $order_rearrange[$child_question->ip_feedback_question_id]['options'][] =  array(  
                        'feedback_options_id' => $child_question->ip_feedback_options_id,
                        'options' => $child_question->options,
                        'options_order_number' => $child_question->options_order_number,
                        'parent_option_id' => $child_question->parent_option_id
                    );
                }  
                
            }
            
            $question->child_questions = $order_rearrange;
            
          
            
            
            foreach($question->child_questions as &$child)
            {
                if(!empty($child['options']))
                 {
                    foreach($child['options'] as &$options){
                            $options_id = $options['feedback_options_id'];
                            // split by , if return array then run in foreach else below functi
                            $parent_id = $child['parent_id'];
                            $option_count_query = "select count(*) as count from ip_feedbacks where parent_id = $parent_id and FIND_IN_SET($options_id, options)";
                            $option_count_result = $wpdb->get_results($option_count_query);
                            $options['option_count'] = $option_count_result[0]->count;
                         }
                    }
            }
            $starting_questions[$key] = $question;
        }
        return array(
            'starting_questions' => $starting_questions,
            //'final_questions' => $final_questions
        );
        
    }
    
    
    */
    
    
    
    
    public static function get_segments_based_on_language($query_args)
    {
        global $wpdb;
        $response = array();
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $language_id = isset($query_args['language_id'])?$query_args['language_id']:1;
        $parent_query = "SELECT * FROM `ip_domains`where `hospital_id` = $hospital_id 
        and `language_id` = $language_id and `enabled_disabled` = 1 ORDER BY  `order_number` ASC";
        $parent_query_result = $wpdb->get_results($parent_query);
        
        $feedback = [];            
        foreach($parent_query_result as $parent_question){
            $domain_id = isset($parent_question->ip_domain_id)? $parent_question->ip_domain_id : 0;
            $question_query = "SELECT * FROM `ip_feedback_questions` t1
                LEFT OUTER JOIN `ip_feedback_options` t2 on t1.ip_feedback_question_id = t2.question_id
                where t1.`hospital_id` = ".$hospital_id."
                AND t1.`language_id` = $language_id AND t1.`domain_id` = $domain_id";
                
            $question_result = $wpdb->get_results($question_query);
            $parent_question->child_questions = $question_result;
            $feedback['starting_questions'][]  = $parent_question;
                
        }
        $feedback = self::format_feedback_data_based_on_language($feedback);
        if(!empty($parent_query_result)){
            return array(
                "status" => true,
                "data" => $feedback,
                "message" => 'Available Feedback Questions data received successfully'
            );
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Questions Available'
        );
    }
    
    
    
    
    public static function format_feedback_data_based_on_language($feedback){
        global $wpdb;
        $starting_questions = isset($feedback['starting_questions'])? $feedback['starting_questions'] : [];
        $final_questions = isset($feedback['final_questions'])? $feedback['final_questions'] : [];
        $available_ratings = [1,2,3,4,5];
        $temp_rating = [];
        $parent_question = [];
        foreach($starting_questions as $key => $question){  
            $child_questions = $question->child_questions;

            $order_rearrange = [];
            foreach($child_questions as $child_question){ 
                
                $order_rearrange[$child_question->ip_feedback_question_id]['feedback_question_id'] = $child_question->ip_feedback_question_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['hospital_id'] = $child_question->hospital_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['domain_id'] = $child_question->domain_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['language_id'] = $child_question->language_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['parent_id'] = $child_question->parent_id;
                $order_rearrange[$child_question->ip_feedback_question_id]['question_type'] = $child_question->question_type;
                $order_rearrange[$child_question->ip_feedback_question_id]['question'] = $child_question->question;
                $order_rearrange[$child_question->ip_feedback_question_id]['enabled_disabled'] = $child_question->enabled_disabled;
                $order_rearrange[$child_question->ip_feedback_question_id]['comment_enabled_disabled'] = $child_question->comment_enabled_disabled;
                $order_rearrange[$child_question->ip_feedback_question_id]['comment_head'] = $child_question->comment_head;
                $order_rearrange[$child_question->ip_feedback_question_id]['created_by'] = $child_question->created_by;
                $order_rearrange[$child_question->ip_feedback_question_id]['created_on'] = $child_question->created_on;
                $order_rearrange[$child_question->ip_feedback_question_id]['question_order_number'] = $child_question->question_order_number;
                
                $parent_id = $child_question->parent_id;
                if($child_question->question_type == 'star_rating')
                {
                    $star_rating_query = "SELECT  `parent_id` ,  `rating` , COUNT(  `rating` ) AS count
                                            FROM  `ip_feedbacks` 
                                            WHERE question_type =  'star_rating' AND parent_id = $parent_id
                                            GROUP BY parent_id, rating";
                    $star_rating_result = $wpdb -> get_results($star_rating_query,ARRAY_A);
                    
                    foreach($star_rating_result as $data){
                        foreach($available_ratings as $rating){
                            if($data['rating'] == $rating){
                                $temp_rating[$child_question->ip_feedback_question_id]['answer_count'][$data['rating']] = $data['count'];
                            }else{
                                if(
                                    empty($temp_rating[$child_question->ip_feedback_question_id]['answer_count'][$rating]) 
                                ){
                                    $temp_rating[$child_question->ip_feedback_question_id]['answer_count'][$rating] = 0;
                                }
                            }
                        }
                        
                    }
                    
                   // $count_array[$rating] = $star_rating_result[0]->count;
                    
                    $order_rearrange[$child_question->ip_feedback_question_id]['answer_count'] = $temp_rating[$child_question->ip_feedback_question_id]['answer_count'];
                }
                
                if(!empty($child_question->ip_feedback_options_id)){
                    $order_rearrange[$child_question->ip_feedback_question_id]['options'][] =  array(  
                        'feedback_options_id' => $child_question->ip_feedback_options_id,
                        'options' => $child_question->options,
                        'options_order_number' => $child_question->options_order_number,
                        'parent_option_id' => $child_question->parent_option_id
                    );
                }  
                
            }
            
            $question->child_questions = $order_rearrange;
            
            /* Options Count
            
            Query = "SELECT t1.parent_id,t2.parent_option_id,count(t1.options) as count FROM `ip_feedbacks` t1 
            JOIN `ip_feedback_options` t2 on t1.parent_id = t2.question_id where t2.ip_feedback_options_id in (t1.options) group by t2.parent_option_id,t2.question_id ";

*/
            
            
            foreach($question->child_questions as &$child)
            {
                if(!empty($child['options']))
                 {
                     //print_r($child['options']);
                    foreach($child['options'] as &$options){
                            $options_id = $options['parent_option_id'];
                            // split by , if return array then run in foreach else below functi
                            $parent_id = $child['parent_id'];
                            $option_count_query = "select count(*) as count from ip_feedbacks where parent_id = $parent_id and FIND_IN_SET($options_id, options)";
                            $option_count_result = $wpdb->get_results($option_count_query);
                            $options['option_count'] = $option_count_result[0]->count;
                         }
                    }
            }
            $starting_questions[$key] = $question;
        }
        return array(
            'starting_questions' => $starting_questions,
            //'final_questions' => $final_questions
        );
        
    }
    
    
    
    
    public static function get_question_wise_report($query_args)
    {
        global $wpdb;
        $question_id = $query_args['question_id'];
        $response = array();
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        
        $table_name = "ip_feedbacks";
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $pagenate_query = '';
        $order_by = '';
        $select_query = " ";
        $search_query='';
        $search_string = isset($query_args['search_string'])?$query_args['search_string'] : "";
        $current_page = isset($query_args['current_page'])? $query_args['current_page'] : 1;
        $record_per_page = isset($query_args['record_per_page'])? $query_args['record_per_page'] : 10;
        $sort_ope = isset($query_args['sort_ope'])?$query_args['sort_ope']:"";
        $sort_name = isset($query_args['sort_name'])?$query_args['sort_name']:"";
        $start_date = isset($query_args['start_date'])?$query_args['start_date']:"";
        $end_date = isset($query_args['end_date'])?$query_args['end_date']:"";
        $between = '';
        
        /**
         *Default for 30days if not selected by user
         * @edited by pradeep
         */
        if(empty($query_args['start_date'])){
            $query_args['start_date'] = date('Y-m-d',strtotime("-30 days"));
        }
        if(empty($query_args['end_date'])){
            $query_args['end_date'] = date('Y-m-d');
        }
        
        if(isset($current_page) && !empty($current_page) && isset($record_per_page) && !empty($record_per_page) ){
            $pagenate_query = Scolaa_Api_Helper::build_pagenate_query($current_page,$record_per_page);    
        }
        
        if(isset($search_string) && !empty($search_string)){
            $search_query = 'AND (';
            $fields = Scolaa_Feedback_V1::get_question_wise_report_fields();
            $count = 0;
            foreach($fields as $key => $field){
                $search_query .= $field . " LIKE '%$search_string%' OR ";
            }
            $search_query = substr($search_query,0,-3).")";
        }

        if(isset($start_date) && isset($end_date) && !empty($start_date) && !empty($end_date)){
            $between = "AND F.date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:23:59'";
        }

        unset($query_args['search_string']);
        unset($query_args['current_page']);
        unset($query_args['record_per_page']);
        unset($query_args['sort_ope']);
        unset($query_args['sort_name']);
        unset($query_args['start_date']);
        unset($query_args['end_date']);
        unset($query_args['question_id']);
        
      if(!empty($sort_ope) && !empty($sort_name)){          
            $order_by = "ORDER BY ".$sort_name." ".$sort_ope;
      }
      else{
          $order_by = "ORDER BY F.date desc";
      }
      if(!empty($query_args)){
          foreach($query_args as $key => $args){
              if($key == 'date'){
                  $select_query .= "AND F.date BETWEEN '".$args." 00:00:00' AND '".$args." 23:23:59'";
              }
              else if($key == 'options'){
                  $select_query .= "AND O.options = '".$args."'";
              }
              else if($key == 'patient_id'){
                  $select_query .= "AND F.patient_id = '".$args."'";
              }
              else{
                   $select_query .= "AND $key = '".$args."'";
              }
          }  
      }
        
      /* $total_count = $wpdb->get_results("select count(*) as total_count from ip_feedbacks F JOIN patient_details P ON F.patient_id = P.patient_id where parent_id = $question_id");
        $data['total_count'] = $total_count[0]->total_count;
        
        $query = "select F.*,P.* from ip_feedbacks F JOIN patient_details P ON F.patient_id = P.patient_id where parent_id = $question_id $select_query $search_query $between $order_by $pagenate_query";
       */
       
       
       $question_type_result = $wpdb -> get_results("select question_type, question from ip_feedback_questions where ip_feedback_question_id = $question_id");
        $question_type = $question_type_result[0]->question_type;
        if($question_type == "star_rating"){
            $total_count = $wpdb->get_results("select count(*) as total_count from ip_feedbacks F JOIN patient_details P ON F.patient_id = P.patient_id where parent_id = $question_id AND F.hospital_id = $hospital_id $select_query $search_query $between");
            $data['total_count'] = $total_count[0]->total_count;
            $query = "select F.rating,F.comments,F.date,P.* from ip_feedbacks F JOIN patient_details P ON F.patient_id = P.patient_id where parent_id = $question_id AND F.hospital_id = $hospital_id $select_query $search_query $between $order_by $pagenate_query";
        }
        else if(($question_type == "radio")||($question_type == "check_box")||($question_type == "select")){
            $total_count = $wpdb->get_results("select count(*) as total_count from ip_feedbacks F JOIN patient_details P ON F.patient_id = P.patient_id JOIN ip_feedback_options O ON F.options=O.ip_feedback_options_id where parent_id = $question_id AND F.hospital_id = $hospital_id $select_query $search_query $between");
            $data['total_count'] = $total_count[0]->total_count;
            $query = "select F.options as option_id,F.comments,F.date,P.*,O.options from ip_feedbacks F JOIN patient_details P ON F.patient_id = P.patient_id JOIN ip_feedback_options O ON F.options=O.ip_feedback_options_id where parent_id= $question_id AND F.hospital_id = $hospital_id $select_query $search_query $between $order_by $pagenate_query";
        }
        else if(($question_type == "text_area")||($question_type == "text")||($question_type == "suggestion")){
            $total_count = $wpdb->get_results("select count(*) as total_count from ip_feedbacks F JOIN patient_details P ON F.patient_id = P.patient_id where parent_id = $question_id AND F.hospital_id = $hospital_id $select_query $search_query $between");
            $data['total_count'] = $total_count[0]->total_count;
            $query = "select F.answer,F.comments,F.date,P.* from ip_feedbacks F JOIN patient_details P ON F.patient_id = P.patient_id where parent_id = $question_id AND F.hospital_id = $hospital_id $select_query $search_query $between $order_by $pagenate_query";
        }
       //print_r($query);
        $result = $wpdb -> get_results($query);
        foreach($result as &$record){
            if($record->patient_mobile_number!=''){
                $record->patient_mobile_number = substr_replace($record->patient_mobile_number,"*******",0,-3);
            }
        }
       if(!empty($result)){
                $data['queried_data']['result'] = $result;
                $data['queried_data']['question'] = $question_type_result[0]->question;
                $data['queried_data']['question_type'] = $question_type_result[0]->question_type;
                $data['queried_count'] = count($result);
                $data['pagination'] = array(
                                        'current_page'=>$current_page,
                                        'record_per_page'=>$record_per_page
                                    );
            return array(
                "status" => true,
                "data" => $data,
                "message" => 'Data for this question received successfully'
            );
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Data Available'
        );
        
    }
    
    
    
    /**
    * Function to get the answers and comments given by a patient based on patient_id
    * @created_by Ranjani
    */
    
    public static function get_patient_full_details($query_args)
    {
        global $wpdb;
        $patient_id = $query_args['patient_id'];
        $response = array();
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        //$patient_result = $wpdb -> get_results("select * from patient_details where patient_id = $patient_id and hospital_id = $hospital_id");
        $patient_result = $wpdb -> get_results("select P.*,L.display as language_used from patient_details P Join language L on P.language_id_used = L.id where P.patient_id = $patient_id and P.hospital_id = $hospital_id");
        if(!empty($patient_result)){
            if($patient_result[0]->patient_mobile_number!=''){
                $patient_result[0]->patient_mobile_number = substr_replace($patient_result[0]->patient_mobile_number,"*******",0,-3);
            }           
            $response['patient_personal_details'] = $patient_result[0];
            $query = "select F.*,Q.question,Q.question_order_number,Q.domain_id,D.domain_name,D.order_number from ip_feedbacks F JOIN ip_feedback_questions Q ON F.parent_id = Q.ip_feedback_question_id JOIN ip_domains D ON D.ip_domain_id = Q.domain_id where patient_id = $patient_id and F.hospital_id = $hospital_id order by D.order_number, Q.question_order_number";
            $result = $wpdb -> get_results($query);
            if(!empty($result)){
                foreach($result as $answer)
                {
                    $temp = array();
                    if($answer->question_type == "star_rating")
                    {
                        $temp['question'] = $answer->question;
                        $temp['question_type'] = $answer->question_type;
                        $temp['answer'] = $answer->rating;
                        $temp['comments'] = $answer->comments;
                        $response['starting_questions'][$answer->order_number]['domain_name'] = $answer->domain_name;
                        $response['starting_questions'][$answer->order_number]['child_questions'][] = $temp;
                        
                    }
                    else if($answer->question_type == "text" || $answer->question_type == "text_area" || $answer->question_type == "suggestion")
                    {
                        $temp['question'] = $answer->question;
                        $temp['question_type'] = $answer->question_type;
                        $temp['answer'] = $answer->answer;
                        $temp['comments'] = $answer->comments;
                        $response['starting_questions'][$answer->order_number]['domain_name'] = $answer->domain_name;
                        $response['starting_questions'][$answer->order_number]['child_questions'][] = $temp;
                    }
                    else if($answer->question_type == "radio" || $answer->question_type == "check_box" || $answer->question_type == "select")
                    {
                        $temp['question'] = $answer->question;
                        $temp['question_type'] = $answer->question_type;
                        $options =  explode(",",$answer->options);
                        $answer_option = "";
                        foreach($options as $option){
                            $answer_result=$wpdb->get_results("select options from ip_feedback_options where ip_feedback_options_id = $option ");
                            $answer_option .= $answer_result[0]->options." , ";
                        }
                        $answer_option = substr($answer_option,0,-2);
                        $temp['answer'] = $answer_option;
                        $temp['comments'] = $answer->comments;
                        $response['starting_questions'][$answer->order_number]['domain_name'] = $answer->domain_name;
                        $response['starting_questions'][$answer->order_number]['child_questions'][] = $temp;
                    }
                }
            }
            $final_query = "select FF.*,FFQ.question from ip_final_feedbacks FF JOIN ip_final_feedback_questions FFQ ON FF.parent_id = FFQ.ip_final_feedback_questions_id where patient_id = $patient_id and FF.hospital_id = $hospital_id order by FFQ.order_number";
            $final_results = $wpdb -> get_results($final_query);
            if(!empty($final_results)){
                foreach($final_results as $answer)
                {
                    $final_temp['question_type'] = $answer->question_type;
                    $final_temp['question'] = $answer->question;
                    $final_temp['answer'] = $answer->rating;
                    $response['final_questions'][$answer->question_type] = $final_temp;
                }
            
                return array(
                    "status" => true,
                    "data" => $response,
                    "message" => "Patient's answers received successfully"
                );
            }
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Data Available'
        );
    }
    
    public static function get_patient_full_details_old_version($query_args)
    {
        global $wpdb;
        $patient_id = $query_args['patient_id'];
        $response = array();
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $patient_result = $wpdb -> get_results("select * from patient_details where patient_id = $patient_id and hospital_id = $hospital_id");
        if(!empty($patient_result)){
            if($patient_result[0]->patient_mobile_number != ''){
                $patient_result[0]->patient_mobile_number = substr_replace($patient_result[0]->patient_mobile_number,"*******",0,-3);
            }
            $response['patient_personal_details'] = $patient_result[0];
            $query = "select F.*,Q.question,Q.question_order_number,Q.domain_id,D.domain_name from ip_feedbacks F JOIN ip_feedback_questions Q ON F.parent_id = Q.ip_feedback_question_id JOIN ip_domains D ON D.ip_domain_id = Q.domain_id where patient_id = $patient_id and F.hospital_id = $hospital_id order by D.order_number, Q.question_order_number";
            $result = $wpdb -> get_results($query);
            if(!empty($result)){
                foreach($result as $answer)
                {
                    $temp = array();
                    if($answer->question_type == "star_rating")
                    {
                        $temp['question'] = $answer->question;
                        $temp['question_type'] = $answer->question_type;
                        $temp['answer'] = $answer->rating;
                        $temp['comments'] = $answer->comments;
                        $response['starting_questions'][$answer->domain_name][] = $temp;
                    }
                    else if($answer->question_type == "text" || $answer->question_type == "text_area" || $answer->question_type == "suggestion")
                    {
                        $temp['question'] = $answer->question;
                        $temp['question_type'] = $answer->question_type;
                        $temp['answer'] = $answer->answer;
                        $temp['comments'] = $answer->comments;
                        $response['starting_questions'][$answer->domain_name][] = $temp;
                    }
                    else if($answer->question_type == "radio" || $answer->question_type == "check_box" || $answer->question_type == "select")
                    {
                        $temp['question'] = $answer->question;
                        $temp['question_type'] = $answer->question_type;
                        $options =  explode(",",$answer->options);
                        $answer_option = "";
                        foreach($options as $option){
                            $answer_result=$wpdb->get_results("select options from ip_feedback_options where ip_feedback_options_id = $option ");
                            $answer_option .= $answer_result[0]->options." , ";
                        }
                        $answer_option = substr($answer_option,0,-2);
                        $temp['answer'] = $answer_option;
                        $temp['comments'] = $answer->comments;
                        $response['starting_questions'][$answer->domain_name][] = $temp;
                    }
                }
            }
            $final_query = "select FF.*,FFQ.question from ip_final_feedbacks FF JOIN ip_final_feedback_questions FFQ ON FF.parent_id = FFQ.ip_final_feedback_questions_id where patient_id = $patient_id and FF.hospital_id = $hospital_id order by FFQ.order_number";
            $final_results = $wpdb -> get_results($final_query);
            if(!empty($final_results)){
                foreach($final_results as $answer)
                {
                    $final_temp['question_type'] = $answer->question_type;
                    $final_temp['question'] = $answer->question;
                    $final_temp['answer'] = $answer->rating;
                    $response['final_questions'][$answer->question_type] = $final_temp;
                }
            
                return array(
                    "status" => true,
                    "data" => $response,
                    "message" => "Patient's answers received successfully"
                );
            }
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Data Available'
        );
    }
    
    public static function create_excel($query_args){
        if(empty($query_args['token'])){
            return "Need Authentication to download";
        }else{
            $user_id = Scolaa_Api_Auth::verify_token($query_args['token'],'api');
            if(empty($user_id)){
                return "Need Authentication to download";
            }
            wp_set_current_user($user_id);
        }
        global $wpdb;
        $language_id=1;
        $filename="Full Report - ".date('d-m-y h-i-s');
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $start_date = isset($query_args['start_date'])?$query_args['start_date']:"";
        $end_date = isset($query_args['end_date'])?$query_args['end_date']:"";
        $between = '';
        if(empty($query_args['start_date'])){
            $query_args['start_date'] = date('Y-m-d',strtotime("-30 days"));
        }
        if(empty($query_args['end_date'])){
            $query_args['end_date'] = date('Y-m-d');
        }
        if(isset($start_date) && isset($end_date) && !empty($start_date) && !empty($end_date)){
            $between = "AND F.date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:23:59'";
        }
        
        $question_query = "Select Q.ip_feedback_question_id,Q.question from ip_domains D 
                            join ip_feedback_questions Q on D.ip_domain_id=Q.domain_id 
                            where D.hospital_id=$hospital_id and D.language_id=$language_id and Q.language_id=$language_id 
                            order by D.order_number,Q.question_order_number";
        $question_result = $wpdb->get_results($question_query);
        $data = array();
        foreach($question_result as $question_title)
        {
            $question_id=$question_title->ip_feedback_question_id;
            $patient_query = "select P.patient_id,patient_name,patient_gender,patient_mobile_number,respondent,mr_number,room_number,P.created_on,F.parent_id,F.question_type,rating,F.answer,F.options,F.comments from ip_feedbacks F 
                                Join  patient_details P on F.patient_id = P.patient_id
                                where F.hospital_id=$hospital_id and F.parent_id=$question_id $between
                                order by P.created_on asc";
            $patient_result = $wpdb->get_results($patient_query);
        //print_r($patient_result);
            foreach($patient_result as $patient_record)
            {
                    $data[$patient_record->patient_id]['patient_name']=$patient_record->patient_name;
                    $data[$patient_record->patient_id]['respondent']=$patient_record->respondent;
                    $data[$patient_record->patient_id]['mr_number']=$patient_record->mr_number;
                    $data[$patient_record->patient_id]['room_number']=$patient_record->room_number;
                    $data[$patient_record->patient_id]['patient_gender']=$patient_record->patient_gender;
                    $data[$patient_record->patient_id]['date']=$patient_record->created_on;
                    $data[$patient_record->patient_id]['patient_mobile_number']=$patient_record->patient_mobile_number;
                    
                    if(($patient_record->question_type == 'star_rating')||($patient_record->question_type == 'emoji')){
                        $data[$patient_record->patient_id][$patient_record->parent_id]=$patient_record->rating;
                    }
                    elseif(($patient_record->question_type == 'text')||($patient_record->question_type == 'text_area')||($patient_record->question_type == 'suggestion')){
                        $data[$patient_record->patient_id][$patient_record->parent_id]=$patient_record->answer;
                    }
                    elseif(($patient_record->question_type == 'check_box')||($patient_record->question_type == 'radio')){
                        $data[$patient_record->patient_id][$patient_record->parent_id]=$patient_record->options;
                    }
                
            }
        }
        
        $phpExcel = new PHPExcel;
        $phpExcel->getDefaultStyle()->getFont()->setName('Calibri');
        $phpExcel->getDefaultStyle()->getFont()->setSize(12);
        $phpExcel ->getProperties()->setTitle("Report");
        $phpExcel ->getProperties()->setCreator("Ranjani");
        $phpExcel ->getProperties()->setDescription("Excel SpreadSheet in PHP");
        $writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
        $sheet = $phpExcel ->getActiveSheet();
        $header_string = "Patient Name,Respondent,MR Number, Room Number,Patient Gender,Date,Patient Mobile Number,";
        foreach($question_result as $result)
        {
            $header_string= $header_string.$result->question.",";
        }
        $header_string = substr($header_string,0,-1);
        $headers=explode(',',$header_string);
        
        $styleArray = array(
        'font'  => array(
        'bold'  => true,
        'size'  => 12,
        'name'  => 'Calibri'
        ));

        
        $col = 'A';
        $row = 1;
        foreach($headers as $question){
            $phpExcel->getActiveSheet()->getColumnDimension($col)
                ->setAutoSize(true);
            $phpExcel->getActiveSheet()->SetCellValue($col.$row, $question);
            $phpExcel->getActiveSheet()->getStyle($col.$row)->applyFromArray($styleArray);
            $phpExcel->getActiveSheet()->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $col++;
        }
        
        
        $row = 2;
        foreach($data as $record){
            $col = 'A';
            foreach($record as $patient_data){
                $phpExcel->getActiveSheet()->getColumnDimension($col)
                    ->setAutoSize(true);
                $phpExcel->getActiveSheet()->SetCellValue($col.$row,$patient_data);
                $phpExcel->getActiveSheet()->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $col++;
            }
            $row++;
        }
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel5'); 
        $objWriter->save('php://output');
        exit();
    }
    
}
Scolaa_Ip_Dashboard::start();