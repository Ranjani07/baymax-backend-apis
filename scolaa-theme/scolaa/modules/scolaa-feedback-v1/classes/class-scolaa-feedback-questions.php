<?php
class Scolaa_Feedback_Questions{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
       
    } 
    
     public static function get_feedback_questions($query_args){
        global $wpdb;
        $table_name = 'feedback_questions';
        $pagenate_query = '';
        $order_by = '';
        $select_query = " ";
        $search_query='';
        $search_string = isset($query_args['search_string'])?$query_args['search_string'] : "";
        $current_page = isset($query_args['current_page'])? $query_args['current_page'] : 1;
        $record_per_page = isset($query_args['record_per_page'])? $query_args['record_per_page'] : 10;
        $sort_ope = isset($query_args['sort_ope'])?$query_args['sort_ope']:"";
        $sort_name = isset($query_args['sort_name'])?$query_args['sort_name']:"";
        
        if(isset($current_page) && !empty($current_page) && isset($record_per_page) && !empty($record_per_page) ){
            $pagenate_query = Scolaa_Api_Helper::build_pagenate_query($current_page,$record_per_page);    
        }
        $search_query = '(';
        if(isset($search_string) && !empty($search_string)){
            $fields = Scolaa_Hospitals_V1::get_feedback_questions_fields();
            
            foreach($fields as $key => $field){
                $search_query .= $field . " LIKE '%$search_string%' OR ";
            }
           
        }
       $search_query .= ' 1=1 )';
        
        unset($query_args['search_string']);
        unset($query_args['current_page']);
        unset($query_args['record_per_page']);
        unset($query_args['sort_ope']);
        unset($query_args['sort_name']);
        
       if(!empty($sort_ope) && !empty($sort_name)){          
            $order_by = "ORDER BY ".$sort_name." ".$sort_ope;
       }
       foreach($query_args as $key => $args){
                $select_query .= " $key = $args AND ";
          
       }  
       
       $select_query .= " enabled_disabled = 1 AND ";
       
        $total_count = $wpdb->get_results("SELECT COUNT(*) as total_count FROM $table_name WHERE $select_query $search_query $order_by $pagenate_query");
        $data['total_count'] = $total_count[0]->total_count;
        $query = "SELECT * FROM $table_name WHERE $select_query $search_query $order_by $pagenate_query";
        
        $queried_data = $wpdb->get_results($query);
       
        $data['queried_data'] = $queried_data;
        $data['queried_count'] = count($queried_data);
        $data['pagination'] = array(
                                'current_page'=>$current_page,
                                'record_per_page'=>$record_per_page
                            );
        return $data; 
        
    }  
    
    public static function insert_feedback_questions($feedback_questions_data){
        global $wpdb;
        $table_name = "feedback_questions"; 
        
        foreach($feedback_questions_data as $key => $value){
                $feedback_questions_data[$key] = sanitize_text_field($value);
        }
        $result = $wpdb->insert($table_name, $feedback_questions_data);
        if(!empty($wpdb->rows_affected)){
            return array(
				"status" => true,
				"message" => "",
				"data" => ["inserted_id"=>$wpdb->insert_id]
			);
        }
		return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
		);       
    }
    
    public static function update_feedback_questions($feedback_questions_data){
        global $wpdb;
        $table_name = "feedback_questions";
        $data = [];
        $where = [];
        foreach($feedback_questions_data as $key => $value){
            if($key == 'feedback_question_id'){
                $where[$key] = $value;
            }
            else{
                    $data[$key] = sanitize_text_field($value);
                }
        }
        $result = $wpdb->update( $table_name, $data, $where);
        if(!empty($wpdb->rows_affected)){
            return array(
				"status" => true,
				"message" => "",
				"data" => ["updated_id"=>$wpdb->insert_id]
			);
        }
		return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
		);      
    }
    
   
   
    public static function delete_feedback_questions($feedback_questions_data){
        global $wpdb;
        $table_name = 'feedback_questions';        
        $where = [];
        foreach($feedback_questions_data as $key => $value){
            if($key == 'feedback_question_id'){
                $where[$key] = $value;
            }
        }
        $update['enabled_disabled'] = 0;
        $result = $wpdb->update($table_name, $update, $where);
        
        if($result !== false && $result){
           $trash_data['table_name'] = $table_name;
            $trash_data['deleted_id'] = $feedback_questions_data['feedback_question_id'];
            $trash_data['deleted_by'] = '';
            $trash_data['reason_for_deletion'] = $feedback_questions_data['reason_for_deletion'];
            $trash_result = $wpdb -> insert('trash',$trash_data); 
            if(!empty($wpdb->rows_affected)){
                return array(
    				'status' => true,
    				'message' => 'deleted successfully',
    				'data' => array('deleted_id' => $wpdb->insert_id)
    			);
            }
        }
    
        return array(
			"status" => false,
			"message"=>$wpdb->last_error,
			'data' => []
	    );		
    }
    
    public static function get_available_feedback_questions($query_args)
    {
        global $wpdb;
        $wpdb->hide_errors();
        $hospital_id = isset($query_args['hospital_id'])? $query_args['hospital_id'] : 0;
        $query = "SELECT id,display 
                  FROM feedback_questions
                    WHERE id
                    IN (
                        SELECT DISTINCT feedback_questions_id
                        FROM domain_translate
                        WHERE hospital_id =$hospital_id
                        AND enabled_disabled =1
                        )
                    OR feedback_questions = 'english'";
        $display_result = $wpdb->get_results($query);
        
        if(!empty($wpdb->last_error)){
            return array(
                "status" => false,
                "data" => $display_result,
                "message" => $wpdb->last_error
            );
        }
        return array(
            "status" => true,
            "data" => $display_result,
            "message" => 'Available Feedback_Questions data received successfully'
        );
        
    }
    
    public static function get_available_ip_feedback_questions($query_args){
        //        die("pradeep");
        global $wpdb;
        $hospital_id = Scolaa_Api_Helper::get_current_user_hospital_id();
        $language_id = isset($query_args['language_id'])? $query_args['language_id'] : 0;
        if(!$hospital_id || !$language_id){
            return array(
                "status" => false,
                "data" => [$hospital_id,$language_id],
                "message" => 'Need correct details.. check your input data'
            );
        }
        /*$result = $wpdb->get_results("SELECT * 
                    FROM  `domains` t1
                    LEFT JOIN  `feedback_questions` t2 ON t2.parent_id = t1.id
                    WHERE t1.`hospital_id` = ".$hospital_id."
                    AND t1.`language_id` = ".$language_id);*/
                    
        
        $parent_query = "SELECT * FROM `domains`where `hospital_id` = $hospital_id 
        and `language_id` = $language_id and `enabled_disabled` = 1 ORDER BY  `order_number` ASC";
        $parent_query_result = $wpdb->get_results($parent_query);
        
        $feedback = [];            
        foreach($parent_query_result as $parent_question){
            $domain_id = isset($parent_question->domain_id)? $parent_question->domain_id : 0;
            $question_query = "SELECT * FROM `feedback_questions` t1
                LEFT OUTER JOIN `feedback_options` t2 on t1.feedback_question_id = t2.question_id
                where t1.`hospital_id` = ".$hospital_id."
                AND t1.`language_id` = ".$language_id." AND t1.`domain_id` = $domain_id";
                
            $question_result = $wpdb->get_results($question_query);
            $parent_question->child_question_options = $question_result;
            $feedback['starting_questions'][]  = $parent_question;
                
        }
        
        $final_query = "SELECT * FROM `final_feedback_questions`where `hospital_id` = $hospital_id and `language_id` = $language_id and `enabled_disabled` = 1";
        $final_query_result = $wpdb->get_results($final_query);
        
        $feedback['final_questions'] = $final_query_result;
        $feedback = self::format_feedback_data($feedback);
        if(!empty($parent_query_result)){
            return array(
                "status" => true,
                "data" => $feedback,
                "message" => 'Available Feedback Questions data received successfully'
            );
        }
        return array(
            "status" => false,
            "data" => [],
            "message" => 'No Questions Available'
        );
    }
    
    public static function format_feedback_data($feedback){
        $starting_questions = isset($feedback['starting_questions'])? $feedback['starting_questions'] : [];
        $final_questions = isset($feedback['final_questions'])? $feedback['final_questions'] : [];
        
        $parent_question = [];
        foreach($starting_questions as $key => $question){  
            $child_questions = $question->child_question_options;
  
            $order_rearrange = [];
            foreach($child_questions as $child_question){ 
                $order_rearrange[$child_question->feedback_question_id]['feedback_question_id'] = $child_question->feedback_question_id;
                $order_rearrange[$child_question->feedback_question_id]['hospital_id'] = $child_question->hospital_id;
                $order_rearrange[$child_question->feedback_question_id]['domain_id'] = $child_question->domain_id;
                $order_rearrange[$child_question->feedback_question_id]['language_id'] = $child_question->language_id;
                $order_rearrange[$child_question->feedback_question_id]['parent_id'] = $child_question->parent_id;
                $order_rearrange[$child_question->feedback_question_id]['question_type'] = $child_question->question_type;
                $order_rearrange[$child_question->feedback_question_id]['question'] = $child_question->question;
                $order_rearrange[$child_question->feedback_question_id]['enabled_disabled'] = $child_question->enabled_disabled;
                $order_rearrange[$child_question->feedback_question_id]['created_by'] = $child_question->created_by;
                $order_rearrange[$child_question->feedback_question_id]['created_on'] = $child_question->created_on;
                $order_rearrange[$child_question->feedback_question_id]['question_order_number'] = $child_question->question_order_number;
                $order_rearrange[$child_question->feedback_question_id]['options'][] =  array(  
                    'feedback_options_id' => $child_question->feedback_options_id,
                    'options' => $child_question->options,
                    'options_order_number' => $child_question->options_order_number
                );
            
            }
            
            $question->child_question_options = $order_rearrange;
            $starting_questions[$key] = $question;
        }
        return array(
            'starting_questions' => $starting_questions,
            'final_questions' => $final_questions
        );
        
    }
    
}
Scolaa_Feedback_Questions::start();