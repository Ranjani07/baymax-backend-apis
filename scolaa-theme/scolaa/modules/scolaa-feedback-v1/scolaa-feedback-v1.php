<?php
class Scolaa_Feedback_V1{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
       self::install_hospital_tables();
     //  Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-patients");
    //   Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-plan-details");
     //  Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-plan-history");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-feedback-language");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-feedback-idiot-domains");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-feedback-domain-translate");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-feedback-questions");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-feedback-options");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-op-domains");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-op-feedback-questions");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-op-feedback-options");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-op-dashboard");
       
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-ip-domains");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-ip-feedback-questions");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-ip-feedback-options");
       Scolaa_Modules::load_class("scolaa-feedback","class-scolaa-ip-dashboard");
    } 
    
    public static function install_hospital_tables(){
        $hospital_query = "CREATE TABLE IF NOT EXISTS `hospitals` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `hospital_name` varchar(100) NOT NULL,
                              `hospital_address1` varchar(100) DEFAULT NULL,
                              `hospital_address2` varchar(100) DEFAULT NULL,
                              `hospital_city` varchar(50) DEFAULT NULL,
                              `hospital_state` varchar(50) DEFAULT NULL,
                              `hospital_country` varchar(50) DEFAULT NULL,
                              `hospital_telephone_number` varchar(20) DEFAULT NULL,
                              `hospital_email_id` varchar(100) NOT NULL,
                              `hospital_logo` text NOT NULL,
                              `current_plan_id` int(5) NOT NULL,
                              `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `created_by` varchar(50) NOT NULL,
                              `modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                              `modified_by` varchar(50) NOT NULL,
                              `enabled_disabled` tinyint(1) NOT NULL DEFAULT '1',
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
                            
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta( $hospital_query );
        
        $plan_details_query = "CREATE TABLE IF NOT EXISTS `plan_details` (
                                  `plan_id` int(5) NOT NULL AUTO_INCREMENT,
                                  `plan_name` varchar(50) NOT NULL,
                                  `plan_price` int(11) NOT NULL,
                                  `plan_details` varchar(500) NOT NULL,
                                  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                  `enabled_disabled` tinyint(1) NOT NULL DEFAULT '1',
                                  PRIMARY KEY (`plan_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        dbDelta( $plan_details_query );
        
        $plan_history_query = "CREATE TABLE IF NOT EXISTS `plan_history` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `plan_id` int(5) NOT NULL,
                                  `plan_price` int(11) NOT NULL,
                                  `reason_for_update` varchar(100) NOT NULL,
                                  `updated_by` varchar(50) NOT NULL,
                                  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        dbDelta( $plan_history_query );
        
        $language_query = "CREATE TABLE IF NOT EXISTS `language` (
                              `id` int(5) NOT NULL AUTO_INCREMENT,
                              `language` varchar(50) NOT NULL,
                              `display` varchar(50) NOT NULL,
                              `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                              `created_by` varchar(50) NOT NULL,
                              `enabled_disabled` tinyint(1) NOT NULL DEFAULT '1',
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        dbDelta( $language_query );
        
        $domains_query = "CREATE TABLE IF NOT EXISTS `domains` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `hospital_id` int(11) NOT NULL,
                              `domain_name` varchar(500) NOT NULL,
                              `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                              `created_by` varchar(50) NOT NULL,
                              `enabled_disabled` tinyint(1) NOT NULL DEFAULT '1',
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        dbDelta($domains_query);
        
        $domain_translate_query = "CREATE TABLE IF NOT EXISTS `domain_translate` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `hospital_id` int(11) NOT NULL,
                                      `domain_id` int(11) NOT NULL,
                                      `language_id` int(5) NOT NULL,
                                      `translated_name` varchar(500) NOT NULL,
                                      `enabled_disabled` tinyint(1) NOT NULL DEFAULT '1',
                                      PRIMARY KEY (`id`)
                                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        dbDelta($domain_translate_query);
        
        $feedback_questions_query = "CREATE TABLE IF NOT EXISTS `feedback_questions` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `hospital_id` int(11) NOT NULL,
                              `domain_id` int(11) NOT NULL,
                              `language_id` int(5) NOT NULL,
                              `english_or_other` int(1) NOT NULL,
                              `english_id` int(11) NOT NULL,
                              `question_type` varchar(100) NOT NULL,
                              `question` varchar(1000) NOT NULL,
                              `enabled_disabled` tinyint(1) NOT NULL DEFAULT '1',
                              `created_by` varchar(50) NOT NULL,
                              `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        dbDelta($feedback_questions_query);
        
        $options_query = "CREATE TABLE IF NOT EXISTS `options` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `question_id` int(11) NOT NULL,
                              `options` varchar(1000) NOT NULL,
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        dbDelta($options_query);

    }
    
    public static function get_hospital_fields(){
        return array(
            'id',
            'hospital_name',
            'hospital_address1',
            'hospital_address2',
            'hospital_city',
            'hospital_state',
            'hospital_country',
            'hospital_telephone_number',
            'hospital_email_id',
            'hospital_logo',
            'current_plan_id',
            'created_on',
            'created_by',
            'modified_on',
            'modified_by',
            'enabled_disabled'
      );
    }
    
    public static function get_plan_details_fields(){
        return array(
            'plan_id',
            'plan_name',
            'plan_price',
            'plan_details',
            'current_time',
            'enabled_disabled'
      );
    }
    
    public static function get_plan_history_fields(){
        return array(
            'id',
            'plan_id',
            'plan_price',
            'reason_for_update',
            'updated_by',
            'updated_on'
      );
    }
    
    public static function get_language_fields(){
        return array(
            'language_id',
            'language',
            'display',
            'created_timestamp',
            'created_by',
            'enabled_disabled'
      );
    }
    
    public static function get_domains_fields(){
        return array(
            'domain_id',
            'hospital_id',
            'domain_name',
            'created_timestamp',
            'created_by',
            'enabled_disabled'
      );
    }
    
    public static function get_feedback_questions_fields(){
        return array(
            'feedback_question_id',
            'hospital_id',
            'domain_id',
            'language_id',
            'english_or_other',
            'english_id',
            'question_type',
            'question',
            'enabled_disabled',
            'created_by',
            'created_on'
      );
    }
    
    public static function get_feedback_options_fields(){
        return array(
            'feedback_options_id',
            'question_id',
            'options'
      );
    }
    
    public static function get_op_domains_fields(){
        return array(
            'op_domain_id',
            'hospital_id',
            'domain_name',
            'created_timestamp',
            'created_by',
            'enabled_disabled'
      );
    }
    
    public static function get_op_feedback_questions_fields(){
        return array(
            'op_feedback_question_id',
            'hospital_id',
            'domain_id',
            'language_id',
            'english_or_other',
            'english_id',
            'question_type',
            'question',
            'enabled_disabled',
            'created_by',
            'created_on'
      );
    }
    
    public static function get_op_feedback_options_fields(){
        return array(
            'feedback_options_id',
            'question_id',
            'options'
      );
    }
    
    public static function get_dashboard_suggestion_fields(){
        return array(
            'F.patient_id',
            'answer',
            'F.date',
            'hospital_patient_id',
            'patient_name',
            'patient_mobile_number',
            'patient_email',
            'patient_gender',
            'mr_number',
            'room_number',
            'created_on',
            'created_by',
            'O.rating'
            );
    }
    
    public static function get_question_wise_report_fields(){
        return array(
            'F.patient_id',
            'F.date',
            'O.options',
            'hospital_patient_id',
            'patient_name',
            'patient_mobile_number',
            'patient_email',
            'patient_gender',
            'mr_number',
            'room_number',
            'created_on',
            'created_by'
        );
    }
    
    public static function file_log($object, $location = 'execution'){
        if(!is_dir(__DIR__.'/log/')){
            mkdir(__DIR__.'/log/');
        }
        $handle = fopen(__DIR__.'/log/'.$location.'.log','a');
        fwrite($handle,PHP_EOL. print_r($object,true));
        fclose($handle);
    }

}
Scolaa_Feedback_V1::start();