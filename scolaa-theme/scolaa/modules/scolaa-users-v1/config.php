<?php

Scolaa_Config::register_module(
    'users',
    array(
        'version' => '1',
        'display_name' => 'Scolaa Users Module',
        'description' => 'Handle all users related actions',
        'enabled' => true,
        'order' => 60,
        'classes' => array(
            'class-key-1' => array(
                'filepath' => '/some-class.php',
                'version' => '1.0.0',
                'display_name' => 'some name',
                'description' => 'Some description',
                'enabled' => true,
            ),
            'class-key-2' => array(
                'filepath' => '/some-folder/some-class.php',
                'version' => '1.0.0',
                'display_name' => 'some name',
                'description' => 'Some description',
                'enabled' => true,
            ),
        ),
    )
);