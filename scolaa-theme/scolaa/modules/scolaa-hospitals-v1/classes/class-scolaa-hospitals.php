<?php
class Scolaa_Hospitals{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
       
    } 
    
    public static function get_hospitals($query_args){
        global $wpdb;
        $wpdb->hide_errors(); 
        $query_args = $query_args;
        $table_name = 'hospitals';
        $pagenate_query = '';
        $order_by = '';
        $select_query = " ";
        $search_query='';
        $search_string = isset($query_args['search_string'])?$query_args['search_string'] : "";
        $current_page = isset($query_args['current_page'])? $query_args['current_page'] : 1;
        $record_per_page = isset($query_args['record_per_page'])? $query_args['record_per_page'] : 10;
        $sort_ope = isset($query_args['sort_ope'])?$query_args['sort_ope']:"";
        $sort_name = isset($query_args['sort_name'])?$query_args['sort_name']:"";
        
        if(isset($current_page) && !empty($current_page) && isset($record_per_page) && !empty($record_per_page) ){
            $pagenate_query = Scolaa_Api_Helper::build_pagenate_query($current_page,$record_per_page);    
        }
        $search_query = '(';
        if(isset($search_string) && !empty($search_string)){
            $fields = Scolaa_Hospitals_V1::get_hospital_fields();
            
            foreach($fields as $key => $field){
                $search_query .= $field . " LIKE '%$search_string%' OR ";
            }
           
        }
       $search_query .= ' 1=1 )';
        
        unset($query_args['search_string']);
        unset($query_args['current_page']);
        unset($query_args['record_per_page']);
        unset($query_args['sort_ope']);
        unset($query_args['sort_name']);
        
       if(!empty($sort_ope) && !empty($sort_name)){          
            $order_by = "ORDER BY ".$sort_name." ".$sort_ope;
       }
       foreach($query_args as $key => $args){
                $select_query .= " $key = $args AND ";
          
       }  
       
       $select_query .= " enabled_disabled = 1 AND ";
       
        $total_count = $wpdb->get_results("SELECT COUNT(*) as total_count FROM $table_name WHERE $select_query $search_query $order_by $pagenate_query");
        $data['total_count'] = $total_count[0]->total_count;
        $query = "SELECT * FROM $table_name WHERE $select_query $search_query $order_by $pagenate_query";
        
        $queried_data = $wpdb->get_results($query);
       
        $data['queried_data'] = $queried_data;
        $data['queried_count'] = count($queried_data);
        $data['pagination'] = array(
                                'current_page'=>$current_page,
                                'record_per_page'=>$record_per_page
                            );
        return $data; 
        
    }  
    
    public static function insert_hospital($hospital_data){
        global $wpdb;
        $wpdb->hide_errors(); 
        $file_name = time();
        $table_name = "hospitals";        
        $hospital_data['hospital_logo'] = isset($_FILES['hospital_logo']['name'])? $_FILES['hospital_logo'] : "";
        if(!empty($hospital_data['hospital_logo'])){
            $hospital_data['hospital_logo'] = $wpdb->_real_escape(self::upload_picture($hospital_data['hospital_logo'],$file_name));
        }
        foreach($hospital_data as $key => $value){
            if($key == 'hospital_website_url'){
                $hospital_data[$key] = $wpdb->_real_escape($value);
            }else{
                $hospital_data[$key] = sanitize_text_field($value);
            }            
        }
        $result = $wpdb->insert($table_name, $hospital_data);
        if(!empty($wpdb->rows_affected)){
            $hospital_id = $wpdb->insert_id;
            $nps_record_query = "INSERT INTO net_promoter_score VALUES('',$hospital_id,'','','','')";
            $nps_result = $wpdb -> get_results($nps_record_query);
            if(!empty($wpdb->rows_affected)){
                return array(
    				"status" => true,
    				"message" => "Hospital data inserted successfully",
    				"data" => ["inserted_id"=>$wpdb->insert_id]
    			);
            }
        }
		return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
		);       
    }
    
    public static function update_hospitals($hospital_data){
        global $wpdb;        
        $wpdb->hide_errors(); 
        $hospital_data['hospital_logo'] = isset($_FILES['hospital_logo']['name'])? $_FILES['hospital_logo'] : '';
        if(!empty($hospital_data['hospital_logo'])){
            $file_name = time();
            $hospital_data['hospital_logo'] = $wpdb->_real_escape(self::upload_picture($hospital_data['hospital_logo'],$file_name));
        }else{
            unset($hospital_data['hospital_logo']);
        }
        $table_name = "hospitals";
        $data = [];
        $where = [];
        foreach($hospital_data as $key => $value){
            if($key == 'id'){
                $where[$key] = $value;
            }
            else{
                if($key == 'hospital_website_url'){
                    $data[$key] = $wpdb->_real_escape($value);
                }else{
                    $data[$key] = sanitize_text_field($value);
                }
            }
        }
        $result = $wpdb->update( $table_name, $data, $where);
        if(!empty($wpdb->rows_affected)){
            return array(
				"status" => true,
				"message" => "",
				"data" => ["updated_id"=>$wpdb->insert_id]
			);
        }
		return array(
				"status" => false,
				"message" => $wpdb->last_error,
		        "data" => []
		);      
    }
    
    public static function upload_picture($picture,$file_name){
        $upload_dir = wp_upload_dir();
        $file_path = $upload_dir['basedir'];
        $picture['name'] = $file_name.'.png';
        $center_path = $file_path. '/hospitals';
        $file_full_path = $file_path. '/hospitals/'.$file_name.'.png';
        if(!file_exists($file_full_path)){
            Scolaa_Api_Helper::init_media_folder($center_path);
            copy( $picture['tmp_name'], $center_path.'/'.$picture['name'] );
            return $upload_dir['baseurl'] . '/hospitals/' .$picture['name'];
        }
        return '';
    }
    
    public static function delete_hospital($hospital_data){
        global $wpdb;
        $table_name = 'hospitals';        
        $where = [];
        foreach($hospital_data as $key => $value){
            if($key == 'id'){
                $where[$key] = $value;
            }
        }
        $update['enabled_disabled'] = 0;
        $result = $wpdb->update($table_name, $update, $where);
        if(!empty($wpdb->rows_affected)){
            $trash_data['table_name'] = $table_name;
            $trash_data['deleted_id'] = $hospital_data['id'];
            $trash_data['deleted_by'] = isset($hospital_data['deleted_by'])?$hospital_data['deleted_by'] : " ";
            $trash_data['reason_for_deletion'] = isset($hospital_data['reason_for_deletion'])?$hospital_data['reason_for_deletion'] : " ";
            $trash_result = $wpdb -> insert('trash',$trash_data);
            if(!empty($wpdb->rows_affected)){
                return array(
    				"status" => true,
    				"message" => "Deleted Successfully",
    				"data" => ["trash_inserted_id"=>$wpdb->insert_id]
    			);
            }
        }
        return array(
			"status" => false,
			"message" => $wpdb->last_error,
	        "data" => []
		);
        
    }
    
     public static function upload_domain_icon($domain_data){
        global $wpdb;
        $file_name = time();
        $domain_data['domain_icon'] = isset($_FILES['domain_icon']['name'])? $_FILES['domain_icon'] : "";
        if(!empty($domain_data['domain_icon'])){
            $domain_data['domain_icon'] = $wpdb->_real_escape(self::upload_domain_picture($domain_data['domain_icon'],$file_name));
        }
        
        if(!empty($domain_data['domain_icon'])){
            return array(
				"status" => true,
				"data" => $domain_data['domain_icon'],
				"message" => ""
			);
        }
		return array(
				"status" => false,
		        "data" => "",
		        "message" => $wpdb->last_error,
		);       
    }
    
    public static function upload_domain_picture($picture,$file_name){
        $upload_dir = wp_upload_dir();
        $file_path = $upload_dir['basedir'];
        $picture['name'] = $file_name.'.png';
        $center_path = $file_path. '/domain_icons';
        $file_full_path = $file_path. '/domain_icons/'.$file_name.'.png';
        if(!file_exists($file_full_path)){
            Scolaa_Api_Helper::init_media_folder($center_path);
            copy( $picture['tmp_name'], $center_path.'/'.$picture['name'] );
            return $upload_dir['baseurl'] . '/domain_icons/' .$picture['name'];
        }
        return '';
    }
    
}
Scolaa_Hospitals::start();