<?php

Scolaa_Config::register_module(
    'hospitals',
    array(
        'version' => '1',
        'display_name' => 'scolaa hospital Module',
        'description' => 'some description',
        'enabled' => true,
        'order' => 10,
        'classes' => array(
            'class-scolaa-hospitals' => array(
                'filepath' => '/class-scolaa-hospitals.php',
                'version' => '1.0.0',
                'display_name' => 'some name',
                'description' => 'Some description',
                'enabled' => true,
            ),
            'class-scolaa-patients' => array(
                'filepath' => '/class-scolaa-hospitals-patients.php',
                'version' => '1.0.0',
                'display_name' => 'some name',
                'description' => 'Some description',
                'enabled' => true,
            )
        ),
    )
);