<?php
class Scolaa_Hospitals_V1{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
       self::install_hospital_tables();
       Scolaa_Modules::load_class("scolaa-hospitals","class-scolaa-hospitals");
     //  Scolaa_Modules::load_class("scolaa-hospitals","class-scolaa-patients");
    //   Scolaa_Modules::load_class("scolaa-hospitals","class-scolaa-plan-details");
     //  Scolaa_Modules::load_class("scolaa-hospitals","class-scolaa-plan-history");
    } 
    
    public static function install_hospital_tables(){
        $hospital_query = "CREATE TABLE IF NOT EXISTS `hospitals` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `hospital_name` varchar(100) NOT NULL,
                              `hospital_address1` varchar(100) DEFAULT NULL,
                              `hospital_address2` varchar(100) DEFAULT NULL,
                              `hospital_city` varchar(50) DEFAULT NULL,
                              `hospital_state` varchar(50) DEFAULT NULL,
                              `hospital_country` varchar(50) DEFAULT NULL,
                              `hospital_telephone_number` varchar(20) DEFAULT NULL,
                              `hospital_email_id` varchar(100) NOT NULL,
                              `hospital_logo` text NOT NULL,
                              `current_plan_id` int(5) NOT NULL,
                              `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `created_by` varchar(50) NOT NULL,
                              `modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                              `modified_by` varchar(50) NOT NULL,
                              `enabled_disabled` tinyint(1) NOT NULL DEFAULT '1',
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
                            
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta( $hospital_query );
        

    }
    
    public static function get_hospital_fields(){
        return array(
            'id',
            'hospital_name',
            'hospital_address1',
            'hospital_address2',
            'hospital_city',
            'hospital_state',
            'hospital_country',
            'hospital_telephone_number',
            'hospital_email_id',
            'hospital_logo',
            'current_plan_id',
            'created_on',
            'created_by',
            'modified_on',
            'modified_by',
            'enabled_disabled'
      );
    }
}
Scolaa_Hospitals_V1::start();