<?php

class Scolaa_Api_Op_Dashboard {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/op-feedback-questions"
         */
        $name_space = 'app/v1/op';
        
        
        register_rest_route($name_space, '/feedback/dashboard/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_dashboard_details'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'hospital_id' => array(
                        'required' => false,
                    )
                )
                
            )
        ));
        
        register_rest_route($name_space, '/feedback/dashboard/nps-graph/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_nps_graph'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'start_date' => array(
                        'required' => false,
                    ),
                    'end_date' => array(
                        'required' => false,
                    ),
                )
                
            )
        ));
        
        register_rest_route($name_space, '/feedback/dashboard/overall-rating-graph/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_overall_rating_graph'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'start_date' => array(
                        'required' => false,
                    ),
                    'end_date' => array(
                        'required' => false,
                    ),
                )
                
            )
        ));
        
        register_rest_route($name_space, '/feedback/dashboard/suggestions/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_suggestions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'hospital_id' => array(
                        'required' => false,
                    )
                )
                
            )
        ));
        
        register_rest_route($name_space, '/feedback/dashboard/segments/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_segments'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'language_id' => array(
                        'required' => false,
                        'default' => 1
                    )
                )
                
            )
        ));
        
         register_rest_route($name_space, '/feedback/dashboard/segments/question-wise-report', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_question_wise_report'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'question_id' => array(
                        'required' => true
                    )
                )
                
            )
        ));
        
        register_rest_route($name_space, '/feedback/dashboard/segments/patients-details', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_patient_full_details'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'patient_id' => array(
                        'required' => true
                    )
                )
                
            )
        ));
        
        register_rest_route($name_space, '/user/profile/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_profile'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'hospital_id' => array(
                        'required' => false,
                    )
                )
                
            )
        ));
        
        
        
    }
    
    public static function get_dashboard_details($request){
        $query_args = $request->get_params();
        $response = Scolaa_Op_Dashboard::get_dashboard_details($query_args);
        if(!empty($response)){
			return Scolaa_Api_Helper::get_success_response('','Dashboard data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No records found', $response);
    }
    
    public static function get_nps_graph($request){
        $query_args = $request->get_params();
        $response = Scolaa_Op_Dashboard::get_nps_graph($query_args);
        if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','NPS graph data received successfully', $response['data']);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_overall_rating_graph($request){
        $query_args = $request->get_params();
        $response = Scolaa_Op_Dashboard::get_overall_rating_graph($query_args);
        if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Overall Rating graph data received successfully', $response['data']);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_suggestions($request){
        $query_args = $request->get_params();
        $response = Scolaa_Op_Dashboard::get_suggestions($query_args);
        if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Suggestions received successfully', $response['data']);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_segments($request){
        $op_feedback_questions_data = $request->get_params();  
        $response = Scolaa_Op_Dashboard::get_segments_based_on_language($op_feedback_questions_data);
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Feedback Questions data', $response['data']);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_question_wise_report($request){
        $op_feedback_questions_data = $request->get_params();  
        $response = Scolaa_Op_Dashboard::get_question_wise_report($op_feedback_questions_data);
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Feedback Questions data', $response['data']);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_patient_full_details($request){
        $patient_data = $request->get_params();  
        $response = Scolaa_Op_Dashboard::get_patient_full_details($patient_data);
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Patient data received successfully', $response['data']);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_profile($request){
        $query_args = $request->get_params();
        $response = Scolaa_Op_Dashboard::get_profile($query_args);
        if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Profile data received successfully', $response['data']);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    
}
Scolaa_Api_Op_Dashboard::start();
