<?php

class Scolaa_Api_Feedback_Questions {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/feedback_questions"
         */
        $name_space = 'app/v1';
        
        register_rest_route($name_space, '/feedback-questions/', array(

		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_feedback_questions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'question_type' => array(
                        'required' => false,
                       
                    ),
                    'question' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'created_on' => array(
                        'required' => false,
                       
                    )
                )
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_feedback_questions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_create_question();
                },
                'args' => array(
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'question_type' => array(
                        'required' => false,
                       
                    ),
                    'question' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'created_on' => array(
                        'required' => false,
                       
                    )
                )
            ),
			array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_delete_question();
                },
                'callback' => array(get_Called_class(), 'delete_feedback_questions'),
                'args' => array(
                    'id' => array(
                        'required' => false,
                    ),                  
                ),
                //'show_in_index'       => false
            ),
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array(get_Called_class(), 'update_feedback_questions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_update_question();
                },
                'args' => array(
                   'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'question_type' => array(
                        'required' => false,
                       
                    ),
                    'question' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'created_on' => array(
                        'required' => false,
                       
                    )    
                )
            )
        ));
        
        register_rest_route($name_space, '/available/ip/feedback/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_available_feedback_questions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'language_id' => array(
                        'required' => true,
                       
                    )
                )
                
            ),
        ));
        
    }
    
    public static function get_feedback_questions($request){
        $query_args = $request->get_params();
        $response = Scolaa_Feedback_Questions::get_feedback_questions($query_args);
        if(isset($response['queried_count']) && !empty($response['queried_count'])){
			return Scolaa_Api_Helper::get_success_response('','Feedback_Questions data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No record matches', $response);
    }
    
    public static function insert_feedback_questions($request){
         $feedback_questions_data = $request->get_params();  
		 $response = Scolaa_Feedback_Questions::insert_feedback_questions($feedback_questions_data);
		 $response['feedback_questions_data'] = $feedback_questions_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Feedback_Questions data inserted successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function delete_feedback_questions($request){
        $feedback_questions_data = $request->get_params();  
        $response = Scolaa_Feedback_Questions::delete_feedback_questions($feedback_questions_data);
        $response['feedback_questions_data'] = $feedback_questions_data;
        if($response['status'] === true){
            return Scolaa_Api_Helper::get_success_response('','Feedback_Questions deleted successfully', $response);
        }
        
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);		
    }
    
    public static function update_feedback_questions($request){
        $feedback_questions_data = $request->get_params();  
        $response = Scolaa_Feedback_Questions::update_feedback_questions($feedback_questions_data);
        $response['feedback_questions_data'] = $feedback_questions_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Feedback_Questions data updated successfully', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_available_feedback_questions($request){
        $feedback_questions_data = $request->get_params();  
        $response = Scolaa_Feedback_Questions::get_available_ip_feedback_questions($feedback_questions_data);
      //  $response['feedback_questions_data'] = $feedback_questions_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Feedback Questions data', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
}
Scolaa_Api_Feedback_Questions::start();


