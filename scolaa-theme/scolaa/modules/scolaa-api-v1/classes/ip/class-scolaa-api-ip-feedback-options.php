<?php

class Scolaa_Api_Ip_Feedback_Options {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/ip-feedback-options"
         */
        $name_space = 'app/v1';
        
                register_rest_route($name_space, '/ip/feedback-options/', array(

		array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_ip_feedback_options'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'id' => array(
                        'required' => false,
                       
                    ),
                    'question_id' => array(
                        'required' => false,
                       
                    ),
                    'options' => array(
                        'required' => false,
                       
                    )
                )
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_ip_feedback_options'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_create_question();
                },
                'args' => array(
                    'question_id' => array(
                        'required' => false,
                       
                    ),
                    'options' => array(
                        'required' => false,
                       
                    )
                )
            ),
			array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_delete_question();
                },
                'callback' => array(get_Called_class(), 'delete_ip_feedback_options'),
                'args' => array(
                    'id' => array(
                        'required' => false,
                    ),                  
                ),
                //'show_in_index'       => false
            ),
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array(get_Called_class(), 'update_ip_feedback_options'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_update_question();
                },
                'args' => array(
                   'id' => array(
                        'required' => false,
                       
                    ),
                    'question_id' => array(
                        'required' => false,
                       
                    ),
                    'options' => array(
                        'required' => false,
                       
                    )    
                )
            )
        ));
    }
    
    public static function get_ip_feedback_options($request){
        $query_args = $request->get_params();
        $response = Scolaa_Ip_Feedback_Options::get_ip_feedback_options($query_args);
        if(isset($response['queried_count']) && !empty($response['queried_count'])){
			return Scolaa_Api_Helper::get_success_response('','Ip_Feedback_Options data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No record matches', $response);
    }
    
    public static function insert_ip_feedback_options($request){
         $ip_feedback_options_data = $request->get_params();  
		 $response = Scolaa_Ip_Feedback_Options::insert_ip_feedback_options($ip_feedback_options_data);
		 $response['ip_feedback_options_data'] = $ip_feedback_options_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Ip_Feedback_Options data inserted successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function delete_ip_feedback_options($request){
        $ip_feedback_options_data = $request->get_params();  
        $response = Scolaa_Ip_Feedback_Options::delete_ip_feedback_options($ip_feedback_options_data);
        $response['ip_feedback_options_data'] = $ip_feedback_options_data;
        if($response['status'] === true){
            return Scolaa_Api_Helper::get_success_response('','Ip_Feedback_Options deleted successfully', $response);
        }
        
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);		
    }
    
    public static function update_ip_feedback_options($request){
        $ip_feedback_options_data = $request->get_params();  
        $response = Scolaa_Ip_Feedback_Options::update_ip_feedback_options($ip_feedback_options_data);
        $response['ip_feedback_options_data'] = $ip_feedback_options_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Ip_Feedback_Options data updated successfully', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
}
Scolaa_Api_Ip_Feedback_Options::start();


