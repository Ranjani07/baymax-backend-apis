<?php

class Scolaa_Api_Ip_Feedback_Questions {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/ip-feedback-questions"
         */
        $name_space = 'app/v1';
        
        register_rest_route($name_space, '/ip/feedback-questions/', array(

		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_ip_feedback_questions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'question_type' => array(
                        'required' => false,
                       
                    ),
                    'question' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'created_on' => array(
                        'required' => false,
                       
                    )
                )
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_ip_feedback_questions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_create_question();
                },
                'args' => array(
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'question_type' => array(
                        'required' => true,
                       
                    ),
                    'question' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'created_on' => array(
                        'required' => false,
                       
                    )
                )
            ),
			array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_delete_question();
                },
                'callback' => array(get_Called_class(), 'delete_ip_feedback_questions'),
                'args' => array(
                    'id' => array(
                        'required' => false,
                    ),                  
                ),
                //'show_in_index'       => false
            ),
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array(get_Called_class(), 'update_ip_feedback_questions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_update_question();
                },
                'args' => array(
                   'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'question_type' => array(
                        'required' => false,
                       
                    ),
                    'question' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'created_on' => array(
                        'required' => false,
                       
                    )    
                )
            )
        ));
        
        register_rest_route($name_space, '/available/ip/feedback/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_available_ip_feedback_questions'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'language_id' => array(
                        'required' => true,
                       
                    )
                )
                
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_ip_feedback'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_question();
                },
                'args' => array(
                    'feedback_details' => array(
                        'required' => true,
                        'validate_callback' => function($param, $request, $key){
                            $realParam = $param;
                            $param = json_decode($param,true);
                            Scolaa_Feedback_V1::file_log([time(),$realParam,$param],"From-request");
                            $starting_questions = $param['starting_questions'];
                            $final_questions = $param['final_questions'];
                            $patient_details = $param['patient_details'];
                            
                            $flag_patient = true;
                            $flag_final = true;
                            $flag_starting = true;
                            
                            $patient_fields = array(
                                'hospital_patient_id',
                                'patient_name',
                                'mobile_number',
                                'email',
                                'respondent',
                                'room_number',
                                'mr_number',
                                'patient_gender',
                                'language_id_used',
                                'patient_photo'
                            );
                            $starting_feedback_fields = array(
                                'parent_id',
                                'question_type',
                                'rating',
                                'options',
                                'answer',
                                'comments',
                                'device_id'
                            );
                            $final_feedback_fields = array(
                                'parent_id',
                                'question_type',
                                'rating',
                                'device_id'
                            );
                            foreach($patient_details as $field => $data){
                                if(!in_array($field,$patient_fields)){
                                    $flag_patient = false;
                                }
                            }
                            /** fields matching for patient details */
                            $question_keys = array_keys($patient_details);
                            if(count($question_keys) != count($patient_fields)){
                                $flag_patient = false;
                            }
                            
                            foreach($starting_questions as $data){
                                $question_keys = array_keys($data);
                                foreach($data as  $field => $value){
                                    if(!in_array($field,$starting_feedback_fields)){
                                        $flag_starting = false;
                                    }
                                }
                                /** fields matching for starting question fields */
                                if(count($question_keys) != count($starting_feedback_fields)){
                                    $flag_starting = false;
                                }
                            }
                            
                            foreach($final_questions as $data){
                                $question_keys = array_keys($data);
                                foreach($data as  $field => $value){
                                    if(!in_array($field,$final_feedback_fields)){
                                        $flag_final = false;
                                    }
                                }
                                /** fields matching for final question fields */
                                if(count($question_keys) != count($final_feedback_fields)){
                                    $flag_final = false;
                                }
                            }
                            Scolaa_Feedback_V1::file_log([time(),$flag_patient,$flag_starting,$flag_final],"From-validate-request");
                            return $flag_patient && $flag_starting && $flag_final;
                        }
                    )
                )
                
            ),
        ));
        
        
    }
    
    public static function get_ip_feedback_questions($request){
        $query_args = $request->get_params();
        $response = Scolaa_Ip_Feedback_Questions::get_ip_feedback_questions($query_args);
        if(isset($response['queried_count']) && !empty($response['queried_count'])){
			return Scolaa_Api_Helper::get_success_response('','Ip_Feedback_Questions data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No record matches', $response);
    }
    
    public static function insert_ip_feedback_questions($request){
         $ip_feedback_questions_data = $request->get_params();  
		 $response = Scolaa_Ip_Feedback_Questions::insert_ip_feedback_questions($ip_feedback_questions_data);
		 $response['ip_feedback_questions_data'] = $ip_feedback_questions_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Ip_Feedback_Questions data inserted successfully', $response['data']);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function delete_ip_feedback_questions($request){
        $ip_feedback_questions_data = $request->get_params();  
        $response = Scolaa_Ip_Feedback_Questions::delete_ip_feedback_questions($ip_feedback_questions_data);
        $response['ip_feedback_questions_data'] = $ip_feedback_questions_data;
        if($response['status'] === true){
            return Scolaa_Api_Helper::get_success_response('','Ip_Feedback_Questions deleted successfully', $response);
        }
        
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);		
    }
    
    public static function update_ip_feedback_questions($request){
        $ip_feedback_questions_data = $request->get_params();  
        $response = Scolaa_Ip_Feedback_Questions::update_ip_feedback_questions($ip_feedback_questions_data);
        $response['ip_feedback_questions_data'] = $ip_feedback_questions_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Ip_Feedback_Questions data updated successfully', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_available_ip_feedback_questions($request){
        $ip_feedback_questions_data = $request->get_params();  
        $response = Scolaa_Ip_Feedback_Questions::get_available_ip_feedback_questions($ip_feedback_questions_data);
      //  $response['ip_feedback_questions_data'] = $ip_feedback_questions_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Feedback Questions data', $response['data']);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function insert_ip_feedback($request){
        $ip_feedback_data = $request->get_param('feedback_details');
        $formatted_data = json_decode($ip_feedback_data,true);
        $process_data = $ip_feedback_data;
        Scolaa_Feedback_V1::file_log([$process_data,json_decode($process_data,true)],"From-request");
		$response = Scolaa_Ip_Feedback_Questions::insert_ip_feedback($formatted_data);
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Ip Feedback data inserted successfully', '');
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], '');
    }
}
Scolaa_Api_Ip_Feedback_Questions::start();
/*

'feedback_details' => array(
    'patient_details' => array(
        'hospital_patient_id' => ""
    	'patient_name' => ""
    	'mobile_number' => ""
    	'email' => ""
    	'patient_photo' => ""    
    ),
    'starting_questions' => array(
        array(
            'parent_id' => ""
            'question_type' => ""
            'rating' => ""
            'options' => ""
            'answer' => ""
            'comments' => ""
            'device_id' => ""
        ),
        array(
            'parent_id' => ""
            'question_type' => ""
            'rating' => ""
            'options' => ""
            'answer' => ""
            'comments' => ""
            'device_id' => ""
        )
    ),
    'final_questions' => array(
        array(
            'parent_id' => ""
            'question_type' => ""
            'rating' => ""
            'device_id' => ""
        ),
        array(
            'parent_id' => ""
            'question_type' => ""
            'rating' => ""
            'device_id' => ""
        ) 
    )
)

*/