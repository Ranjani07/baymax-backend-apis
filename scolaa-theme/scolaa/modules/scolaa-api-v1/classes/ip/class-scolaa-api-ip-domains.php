<?php

class Scolaa_Api_Ip_Domains {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/ip-domains"
         */
        $name_space = 'app/v1';
        
                register_rest_route($name_space, '/ip/domains/', array(

		array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_ip_domains'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_read_ip_domains();
                },
                'args' => array(
                    'ip_domain_id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_name' => array(
                        'required' => false,
                       
                    ),
                    'created_timestamp' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )
                )
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_ip_domains'),
                // 'permission_callback' => function (){
                //     return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_insert_ip_domains();
                // },
                'args' => array(
                    'hospital_id' => array(
                        'required' => true,
                       
                    ),
                    'language_id' => array(
                        'required' => true,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_name' => array(
                        'required' => true,
                       
                    ),
                    'created_timestamp' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => true,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )
                )
            ),
			array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_delete_ip_domains();
                },
                'callback' => array(get_Called_class(), 'delete_ip_domains'),
                'args' => array(
                    'ip_domain_id' => array(
                        'required' => false,
                    ),                  
                ),
                //'show_in_index'       => false
            )
           
        ));
        
      
         register_rest_route($name_space, '/ip/domains/update/', array(
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array(get_Called_class(), 'update_ip_domains'),
                'permission_callback' => array('Scolaa_Api_Helper', 'check_user_logged_in'),
                'args' => array(
                   'ip_domain_id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_name' => array(
                        'required' => false,
                       
                    ),
                    'created_timestamp' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )    
                )
            )
        ));
    }
    
    public static function get_ip_domains($request){
        $query_args = $request->get_params();
        $response = Scolaa_Ip_Domains::get_ip_domains($query_args);
        if(isset($response['queried_count']) && !empty($response['queried_count'])){
			return Scolaa_Api_Helper::get_success_response('','Ip_Domains data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No record matches', $response);
    }
    
    public static function insert_ip_domains($request){
         $ip_domains_data = $request->get_params();  
		 $response = Scolaa_Ip_Domains::insert_ip_domains($ip_domains_data);
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Ip_Domains data inserted successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function delete_ip_domains($request){
        $ip_domains_data = $request->get_params();  
        $response = Scolaa_Ip_Domains::delete_ip_domains($ip_domains_data);
        if($response['status'] === true){
            return Scolaa_Api_Helper::get_success_response('','Ip_Domains deleted successfully', $response);
        }
        
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);		
    }
    
    public static function update_ip_domains($request){
        $ip_domains_data = $request->get_params();  
        $response = Scolaa_Ip_Domains::update_ip_domains($ip_domains_data);
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Ip_Domains data updated successfully', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
}
Scolaa_Api_Ip_Domains::start();


