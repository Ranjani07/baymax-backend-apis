<?php

class Scolaa_Api_Language {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/language"
         */
        $name_space = 'app/v1';
        
                register_rest_route($name_space, '/language/', array(

		array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_language'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_read_language();
                },
                'args' => array(
                    'id' => array(
                        'required' => false,
                       
                    ),
                    'language' => array(
                        'required' => false,
                       
                    ),
                    'display' => array(
                        'required' => false,
                       
                    ),
                    'created_timestamp' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    )
                )
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_language'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_insert_language();
                },
                'args' => array(
                    'language' => array(
                        'required' => true,
                       
                    ),
                    'display' => array(
                        'required' => true,
                       
                    ),
                    'created_timestamp' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )
                )
            ),
			array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_delete_language();
                },
                'callback' => array(get_Called_class(), 'delete_language'),
                'args' => array(
                    'id' => array(
                        'required' => false,
                    ),                  
                ),
                //'show_in_index'       => false
            ),
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array(get_Called_class(), 'update_language'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_update_language();
                },
                'args' => array(
                   'id' => array(
                        'required' => false,
                       
                    ),
                    'language' => array(
                        'required' => false,
                       
                    ),
                    'display' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )    
                )
            )
        ));   
        
        register_rest_route($name_space, '/available/op/language', array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_available_op_languages'),
                'permission_callback' => array('Scolaa_Api_Helper', 'check_user_logged_in'),
                'args' => array(
                   'hospital_id' => array(
                        'required' => false
                       
                    ) 
                )
            )
        ));   
        
        register_rest_route($name_space, '/available/ip/language', array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_available_ip_languages'),
                'permission_callback' => array('Scolaa_Api_Helper', 'check_user_logged_in'),
                'args' => array(
                   'hospital_id' => array(
                        'required' => false
                       
                    ) 
                )
            )
        ));   
    }
    
    public static function get_language($request){
        $query_args = $request->get_params();
        $response = Scolaa_Language::get_language($query_args);
        if(isset($response['queried_count']) && !empty($response['queried_count'])){
			return Scolaa_Api_Helper::get_success_response('','Language data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No record matches', $response);
    }
    
    public static function insert_language($request){
         $language_data = $request->get_params();  
		 $response = Scolaa_Language::insert_language($language_data);
		 $response['language_data'] = $language_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Language data inserted successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function delete_language($request){
        $language_data = $request->get_params();  
        $response = Scolaa_Language::delete_language($language_data);
        $response['language_data'] = $language_data;
        if($response['status'] === true){
            return Scolaa_Api_Helper::get_success_response('','Language deleted successfully', $response);
        }
        
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);		
    }
    
    public static function update_language($request){
        $language_data = $request->get_params();  
        $response = Scolaa_Language::update_language($language_data);
        $response['language_data'] = $language_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Language data updated successfully', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_available_op_languages($request){
        $query_args = $request->get_params();
        $response = Scolaa_Language::get_available_op_languages($query_args);
        if(!empty($response['status'])){
			return Scolaa_Api_Helper::get_success_response('',$response['message'], $response['data']);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_available_ip_languages($request){
        $query_args = $request->get_params();
        $response = Scolaa_Language::get_available_ip_languages($query_args);
        if(!empty($response['status'])){
			return Scolaa_Api_Helper::get_success_response('',$response['message'], $response['data']);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
}
Scolaa_Api_Language::start();


