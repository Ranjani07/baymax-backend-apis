<?php

class Scolaa_Api_Helper {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
    public static function get_error_response($type, $message, $data = array()) {
        $function_name = debug_backtrace()[1]['function'];
        $code = $type . '_' . $function_name . '_error';

        $result = array(
            'code' => $code,
            'data' => isset($data) ? $data : '',
            'message' => $message
        );
        return $result;
    }

    public static function get_success_response($type, $message, $data = array()) {
        $function_name = debug_backtrace()[1]['function'];
        if ($type !== '') {
            $code = $type . '_' . $function_name . '_success';
        } else {
            $code = $function_name . '_success';
        }


        $result = array(
            'code' => $code,
            'data' => isset($data) ? $data : '',
            'message' => $message
        );
        return $result;
    }
    
    public static function register_routes(){
        
        $namespace = 'site';

    }
    
    public static function check_user_logged_in() {

        return is_user_logged_in();
    }
    
   public static function logout() {
        wp_logout();
        return self::get_success_response('', 'Sair com êxito');
    }
 
    
    public static function limited_string_length($param, $request, $key){
        if(strlen($param) > 50 || is_numeric($param)){
            return false;
        }
        return true;    
    }
    
    public static function limited_integer_length($param, $request, $key){
        if(!is_numeric($param) || strlen($param) > 20){
            return false;
        }
        return true;   
    }
    
    public static function limited_float_length($param, $request, $key){
        if(!is_float( floatval($param) ) || strlen($param) > 20){
            return false;
        }
        return true;   
    }
    
     public static function get_limited_float_length($param, $request, $key){
        if(!is_float( floatval($param) ) || strlen($param) > 20){
            return false;
        }
        return true;   
    }
    
    
    
    public static function validate_age_length($param, $request, $key){
        if (strlen($param) < 4 && is_numeric($param) ) {
            return true;
        } 
        return false;
    }
    
    public static function validate_sex_length($param, $request, $key){
        if (strlen($param) == 1 && !is_numeric($param)) {
            return true;
        } 
        return false;
    }
    
    public static function validate_client_type($param, $request, $key){
        if($param == 'client' || $param == 'prospect' || $param == 'all'){
            return true;
        }
        return false;
    }
    
    public static function build_pagenate_query($current_page = 1 ,$record_per_page = 10 ){
        
        if($current_page!== null && $record_per_page !== null){
            
            $start_row = $current_page * $record_per_page - $record_per_page;
            
            $build_query = "LIMIT $start_row , $record_per_page";  
            return  $build_query;
        }
        
       $build_query = "LIMIT 0 , 10"; 
       return  $build_query;
    }
    
    public static function validate_date($param, $request, $key){
         $date_str  = explode('/', $param);
         return checkdate($date_str[1], $date_str[0], $date_str[2]);
        
        
    }
    
    public static function validate_time($param, $request, $key){
        if (preg_match('/^\d{1,2}:\d{1,2}$/', $param)) {
            $match = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $param);
            return $match;
        }
        
        return false;
        
    }
	
	public static function validate_date_event($param){
        $date_str  = explode('/', $param);
        $formated = checkdate($date_str[1], $date_str[0], $date_str[2]);
        if($formated === false){
            return false;
        }
        $formeted_date = date("Y-m-d", strtotime(str_replace('/', '-',$param)));
        return $formeted_date;
        
    }
    
    public static function validate_time_event($param){
        if (preg_match('/^\d{1,2}:\d{1,2}$/', $param)) {
            $match = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $param);
            if($match){
                $formeted_time = date("Y-m-d H:i:s", strtotime($param));
                return $formeted_time;
            }
            
            
        }
        
        return false;
        
    }
    
    public static function validate_get_time_event($param){
        if (preg_match('/^\d{1,2}:\d{1,2}$/', $param)) {
            $match = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $param);
            if($match){
                $formeted_time = date("H:i:s", strtotime($param));
                return $formeted_time;
            }
            
            
        }
        
        return false;
        
    }
    
    public static function init_media_folder($center_path){
        if(!is_dir($center_path)){
            mkdir($center_path,755);  
        }
    }
    
    public static function can_get_question(){
        return current_user_can('can_read_questions');
    }
    
    public static function can_create_question(){
        return current_user_can('can_create_questions');
    }
    
    public static function can_update_question(){
        return current_user_can('can_update_questions');
    }
    
    public static function can_delete_question(){
        return current_user_can('can_delete_questions');
    }
        
    public static function can_insert_feedback(){
        return current_user_can('can_insert_feedback');
    }
        
    public static function can_update_feedback(){
        return current_user_can('can_update_feedback');
    }
        
    public static function can_delete_feedback(){
        return current_user_can('can_delete_feedback');
    }
        
    public static function can_insert_hospital(){
        return current_user_can('can_insert_hospital');
    }
        
    public static function can_update_hospital(){
        return current_user_can('can_update_hospital');
    }
        
    public static function can_delete_hospital(){
        return current_user_can('can_delete_hospital');
    }
        
    public static function can_read_feedback(){
        return current_user_can('can_read_feedback');
    }
        
    public static function can_read_hospital(){
        return current_user_can('can_read_hospital');
    }
        
    public static function can_read_plan(){
        return current_user_can('can_read_plan');
    }
        
    public static function can_insert_plan(){
        return current_user_can('can_insert_plan');
    }
        
    public static function can_update_plan(){
        return current_user_can('can_update_plan');
    }
        
    public static function can_delete_plan(){
        return current_user_can('can_delete_plan');
    }
        
    public static function can_read_plan_history(){
        return current_user_can('can_read_plan_history');
    }
        
    public static function can_insert_plan_history(){
        return current_user_can('can_insert_plan_history');
    }
        
    public static function can_update_plan_history(){
        return current_user_can('can_update_plan_history');
    }
        
    public static function can_delete_plan_history(){
        return current_user_can('can_delete_plan_history');
    }
        
    public static function can_read_department(){
        return current_user_can('can_read_department');
    }
        
    public static function can_insert_department(){
        return current_user_can('can_insert_department');
    }
        
    public static function can_update_department(){
        return current_user_can('can_update_department');
    }
        
    public static function can_delete_department(){
        return current_user_can('can_delete_department');
    }
        
    public static function can_import_patient(){
        return current_user_can('can_import_patient');
    }
        
    public static function can_read_patient(){
        return current_user_can('can_read_patient');
    }
        
    public static function can_update_patient(){
        return current_user_can('can_update_patient');
    }
        
    public static function can_insert_patient(){
        return current_user_can('can_insert_patient');
    }
        
    public static function can_delete_patient(){
        return current_user_can('can_delete_patient');
    }
        
    public static function can_create_user(){
        return current_user_can('can_create_user');
    }
        
    public static function can_update_user(){
        return current_user_can('can_update_user');
    }
        
    public static function can_delete_user(){
        return current_user_can('can_delete_user');
    }
    
    public static function can_read_language(){
        return current_user_can('can_read_plan');
    }
        
    public static function can_insert_language(){
        return current_user_can('can_insert_plan');
    }
        
    public static function can_update_language(){
        return current_user_can('can_update_plan');
    }
        
    public static function can_delete_language(){
        return current_user_can('can_delete_plan');
    }
    
    public static function can_get_areas(){
        return current_user_can('can_read_areas');
    }
    
    public static function get_current_user_hospital_id(){
        global $wpdb;
        $user_id = get_current_user_id();
        $query = "SELECT * FROM `user_details` WHERE `wordpress_user_id` = ".$user_id;
        $results = $wpdb->get_results($query);
        
        if(!empty($results)){
            return isset($results[0]->hospital_id)? $results[0]->hospital_id : 0;
        }
        return 0;
    }
    
}
Scolaa_Api_Helper::start();


