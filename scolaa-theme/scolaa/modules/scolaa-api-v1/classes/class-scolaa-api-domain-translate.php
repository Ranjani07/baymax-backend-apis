<?php

class Scolaa_Api_Domain_Translate {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/domain_translate"
         */
        $name_space = 'app/v1';
        
                register_rest_route($name_space, '/domain-translate/', array(

		array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_domain_translate'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_read_domain_translate();
                },
                'args' => array(
                    'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'translated_name' => array(
                        'required' => false,
                       
                    )
                )
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_domain_translate'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_insert_domain_translate();
                },
                'args' => array(
                    
                    'hospital_id' => array(
                        'required' => true,
                       
                    ),
                    'domain_id' => array(
                        'required' => true,
                       
                    ),
                    'language_id' => array(
                        'required' => true,
                       
                    ),
                    'translated_name' => array(
                        'required' => true,
                       
                    )
                )
            ),
			array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_delete_domain_translate();
                },
                'callback' => array(get_Called_class(), 'delete_domain_translate'),
                'args' => array(
                    'id' => array(
                        'required' => false,
                    ),                  
                ),
                //'show_in_index'       => false
            )
           
        ));
        
      
         register_rest_route($name_space, '/domain-translate/update/', array(
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array(get_Called_class(), 'update_domain_translate'),
                'permission_callback' => array('Scolaa_Api_Helper', 'check_user_logged_in'),
                'args' => array(
                   'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'translated_name' => array(
                        'required' => false,
                       
                    )    
                )
            )
        ));   
    }
    
    public static function get_domain_translate($request){
        $query_args = $request->get_params();
        $response = Scolaa_Domain_Translate::get_domain_translate($query_args);
        if(isset($response['queried_count']) && !empty($response['queried_count'])){
			return Scolaa_Api_Helper::get_success_response('','Domain_Translate data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No record matches', $response);
    }
    
    public static function insert_domain_translate($request){
         $domain_translate_data = $request->get_params();  
		 $response = Scolaa_Domain_Translate::insert_domain_translate($domain_translate_data);
		 $response['domain_translate_data'] = $domain_translate_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Domain_Translate data inserted successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function delete_domain_translate($request){
        $domain_translate_data = $request->get_params();  
        $response = Scolaa_Domain_Translate::delete_domain_translate($domain_translate_data);
        $response['domain_translate_data'] = $domain_translate_data;
        if($response['status'] === true){
            return Scolaa_Api_Helper::get_success_response('','Domain_Translate deleted successfully', $response);
        }
        
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);		
    }
    
    public static function update_domain_translate($request){
        $domain_translate_data = $request->get_params();  
        $response = Scolaa_Domain_Translate::update_domain_translate($domain_translate_data);
        $response['domain_translate_data'] = $domain_translate_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Domain_Translate data updated successfully', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
}
Scolaa_Api_Domain_Translate::start();


