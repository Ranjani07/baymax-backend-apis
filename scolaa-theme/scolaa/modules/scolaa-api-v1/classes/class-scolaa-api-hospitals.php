<?php

class Scolaa_Api_Hospitals {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/hospital"
         */
        $name_space = 'app/v1';
        
                register_rest_route($name_space, '/hospitals/', array(

		array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_hospitals'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_read_hospital();
                },
                'args' => array(
                    'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_name' => array(
                        'required' => false,
                       
                    ),
                    'hospital_address1' => array(
                        'required' => false,
                       
                    ),
                    'hospital_address2' => array(
                        'required' => false,
                       
                    ),
                    'hospital_city' => array(
                        'required' => false,
                       
                    ),
                    'hospital_state' => array(
                        'required' => false,
                       
                    ),
                    'hospital_country' => array(
                        'required' => false,
                       
                    ),
                    'hospital_telephone_number' => array(
                        'required' => false,
                       
                    ),
                    'hospital_email_id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_logo' => array(
                        'required' => false,
                       
                    ),
                    'current_plan_id' => array(
                        'required' => false,
                       
                    ),
                    'created_on' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'modified_on' => array(
                        'required' => false,
                       
                    ),
                    'modified_by' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )
                )
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'create_hospital'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_insert_hospital();
                },
                'args' => array(
                    'hospital_name' => array(
                        'required' => true,
                       
                    ),
                    'hospital_address1' => array(
                        'required' => true,
                       
                    ),
                    'hospital_address2' => array(
                        'required' => true,
                       
                    ),
                    'hospital_city' => array(
                        'required' => true,
                       
                    ),
                    'hospital_state' => array(
                        'required' => true,
                       
                    ),
                    'hospital_country' => array(
                        'required' => true,
                       
                    ),
                    'hospital_telephone_number' => array(
                        'required' => true,
                       
                    ),
                    'hospital_email_id' => array(
                        'required' => true,
                       
                    ),
                    'hospital_logo' => array(
                        'required' => false,
                       
                    ),
                    'current_plan_id' => array(
                        'required' => true,
                       
                    ),
                    'created_by' => array(
                        'required' => true,
                       
                    ),
                    'modified_by' => array(
                        'required' => true,
                       
                    )
                )
            ),
			array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_delete_hospital();
                },
                'callback' => array(get_Called_class(), 'delete_hospital'),
                'args' => array(
                    'id' => array(
                        'required' => false,
                    ), 
                    'reason_for_deletion' => array(
                        'required' => false,
                    ), 
                ),
                'show_in_index'       => false
            )
           
        ));
        
      
         register_rest_route($name_space, '/hospitals/update/', array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'update_hospital'),
                'permission_callback' => array('Scolaa_Api_Helper', 'check_user_logged_in'),
                'args' => array(
                   'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_name' => array(
                        'required' => false,
                       
                    ),
                    'hospital_address1' => array(
                        'required' => false,
                       
                    ),
                    'hospital_address2' => array(
                        'required' => false,
                       
                    ),
                    'hospital_city' => array(
                        'required' => false,
                       
                    ),
                    'hospital_state' => array(
                        'required' => false,
                       
                    ),
                    'hospital_country' => array(
                        'required' => false,
                       
                    ),
                    'hospital_telephone_number' => array(
                        'required' => false,
                       
                    ),
                    'hospital_email_id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_logo' => array(
                        'required' => false,
                       
                    ),
                    'current_plan_id' => array(
                        'required' => false,
                       
                    ),
                    'modified_by' => array(
                        'required' => false,
                       
                    )    
                )
            )
        ));   
        
        register_rest_route($name_space, '/domains/upload-domain-icon', array(
		    array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'upload_domain_icon'),
                'permission_callback' => array('Scolaa_Api_Helper', 'check_user_logged_in'),
                'args' => array(
                    'domain_icon' => array(
                        'required' => false,
                       
                    )
                )
                
            ),
        ));
    }
    
    public static function get_hospitals($request){
        $query_args = $request->get_params();
        $response = Scolaa_Hospitals::get_hospitals($query_args);
        if(isset($response['queried_count']) && !empty($response['queried_count'])){
			return Scolaa_Api_Helper::get_success_response('','Hospital data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No record matches', $response);
    }
    
    public static function create_hospital($request){
         $hospital_data = $request->get_params();  
		 $response = Scolaa_Hospitals::insert_hospital($hospital_data);
		 $response['hospital_data'] = $hospital_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Hospital data inserted successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function delete_hospital($request){
        $hospital_data = $request->get_params();  
        $response = Scolaa_Hospitals::delete_hospital($hospital_data);
        $response['hospital_data'] = $hospital_data;
        if($response['status'] === true){
            return Scolaa_Api_Helper::get_success_response('','hospital deleted successfully', $response);
        }
        
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);		
    }
    
    public static function update_hospital($request){
        $hospital_data = $request->get_params();  
        $response = Scolaa_Hospitals::update_hospitals($hospital_data);
        $response['hospital_data'] = $hospital_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','hospital data updated successfully', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
     
    public static function upload_domain_icon($request){
         $op_domains_data = $request->get_params();  
		 $response = Scolaa_Hospitals::upload_domain_icon($op_domains_data);
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Domain Icon uploaded successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
}
Scolaa_Api_Hospitals::start();


