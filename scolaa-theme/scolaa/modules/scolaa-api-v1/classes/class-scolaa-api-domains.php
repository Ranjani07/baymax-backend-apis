<?php

class Scolaa_Api_Domains {
    
    function __construct() {
        self::start();
    }
    
    public static function start(){
        add_action('rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    public static function register_routes(){
        
        /**
         * All name space should be under "app/v1"
         * after that need to specify the service
         * name "/domains"
         */
        $name_space = 'app/v1';
        
                register_rest_route($name_space, '/domains/', array(

		array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_domains'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_read_domains();
                },
                'args' => array(
                    'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_name' => array(
                        'required' => false,
                       
                    ),
                    'created_timestamp' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )
                )
            ),
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_domains'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_insert_domains();
                },
                'args' => array(
                    'hospital_id' => array(
                        'required' => true,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_name' => array(
                        'required' => true,
                       
                    ),
                    'created_timestamp' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )
                )
            ),
			array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_delete_domains();
                },
                'callback' => array(get_Called_class(), 'delete_domains'),
                'args' => array(
                    'id' => array(
                        'required' => false,
                    ),                  
                ),
                //'show_in_index'       => false
            )
           
        ));
        
      
         register_rest_route($name_space, '/domains/update/', array(
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array(get_Called_class(), 'update_domains'),
                'permission_callback' => array('Scolaa_Api_Helper', 'check_user_logged_in'),
                'args' => array(
                   'id' => array(
                        'required' => false,
                       
                    ),
                    'hospital_id' => array(
                        'required' => false,
                       
                    ),
                    'language_id' => array(
                        'required' => false,
                       
                    ),
                    'parent_id' => array(
                        'required' => false,
                       
                    ),
                    'domain_name' => array(
                        'required' => false,
                       
                    ),
                    'created_timestamp' => array(
                        'required' => false,
                       
                    ),
                    'created_by' => array(
                        'required' => false,
                       
                    ),
                    'enabled_disabled' => array(
                        'required' => false,
                       
                    )    
                )
            )
        )); 
        
         register_rest_route($name_space, '/available/op/areas/', array(
		    array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array(get_Called_class(), 'get_available_op_areas'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_questions();
                },
                'args' => array(
                    'language_id' => array(
                        'required' => false,
                       
                    )
                )
                
            ),
        ));
        
        register_rest_route($name_space, '/op/review', array(
		    array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_Called_class(), 'insert_op_reviews'),
                'permission_callback' => function (){
                    return Scolaa_Api_Helper::check_user_logged_in() && Scolaa_Api_Helper::can_get_questions();
                },
                'args' => array(
                    'gender' => array(
                        'required' => true,
                       
                    ),
                    'age_group' => array(
                        'required' => true,
                       
                    ),
                    'name' => array(
                        'required' => false,
                       
                    ),
                    'phone_number' => array(
                        'required' => false,
                       
                    ),
                    'happy_or_not' => array(
                        'required' => true,
                       
                    ),
                    'problematic_areas' => array(
                        'required' => false,
                       
                    ),
                    'further_comments' => array(
                        'required' => false,
                       
                    ),
                    'device_id' => array(
                        'required' => true,
                       
                    )
                )
                
            ),
        ));
    }
    
    public static function get_domains($request){
        $query_args = $request->get_params();
        $response = Scolaa_Domains::get_domains($query_args);
        if(isset($response['queried_count']) && !empty($response['queried_count'])){
			return Scolaa_Api_Helper::get_success_response('','Domains data received successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid','No record matches', $response);
    }
    
    public static function insert_domains($request){
         $domains_data = $request->get_params();  
		 $response = Scolaa_Domains::insert_domains($domains_data);
		 $response['domains_data'] = $domains_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Domains data inserted successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function delete_domains($request){
        $domains_data = $request->get_params();  
        $response = Scolaa_Domains::delete_domains($domains_data);
        $response['domains_data'] = $domains_data;
        if($response['status'] === true){
            return Scolaa_Api_Helper::get_success_response('','Domains deleted successfully', $response);
        }
        
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);		
    }
    
    public static function update_domains($request){
        $domains_data = $request->get_params();  
        $response = Scolaa_Domains::update_domains($domains_data);
        $response['domains_data'] = $domains_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','Domains data updated successfully', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function get_available_op_areas($request){
        $op_areas_data = $request->get_params();  
        $response = Scolaa_Domains::get_available_op_areas($op_areas_data);
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','OP areas data', $response);
        }
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
    
    public static function insert_op_reviews($request){
         $op_reviews_data = $request->get_params();  
		 $response = Scolaa_Domains::insert_op_reviews($op_reviews_data);
		 $response['$op_reviews_data'] = $op_reviews_data;
		if($response['status'] === true){
			return Scolaa_Api_Helper::get_success_response('','OP Reviews inserted successfully', $response);
        }		
        return Scolaa_Api_Helper::get_error_response('invalid',$response['message'], $response['data']);
    }
}
Scolaa_Api_Domains::start();


