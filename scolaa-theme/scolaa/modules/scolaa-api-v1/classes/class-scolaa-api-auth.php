<?php
class Scolaa_Api_Auth{
    
    public static function start(){        
        add_filter('rest_authentication_errors', array(get_called_class(), 'custom_authentication'), 999, 1);        
        add_action( 'rest_api_init', array(get_called_class(), 'register_routes'));
    }
    
   
    
    public static function custom_authentication($result ){
        $token = self::get_scolaa_auth_header();
       
        if($token === false){
            return $result;
        }
        
        $user_id = self::verify_token($token);
        
        if($user_id === false){
            return $result;
        }
        wp_set_current_user($user_id);
        return true;
    }
    
    public static function get_scolaa_auth_header() { 
        
        foreach ($_SERVER as $name => $value) 
        { 
           if ($name === 'HTTP_SCOLAA_AUTHORIZATION'){ 
               return $value; 
           }
        }
        if($_SERVER['REQUEST_URI'] === '/wp-json/adm/report/product/bulk/?type=download'){
            if(isset($_COOKIE['scolaa_authorization'])){
                return $_COOKIE['scolaa_authorization'];
            }
        }
        return false; 
    } 
    
    public static function generate_token($user, $action = 'api'){
        if($user === false){
            return false;
        }
        return $user->ID . '|' . password_hash(self::get_token_string($user, $action), PASSWORD_BCRYPT);   
    }
    
    public static function get_token_string($user, $action = 'api'){
        if($action === 'api'){
            return $user->user_email . $user->user_login . $user->user_registered . $user->user_pass;
        }
        return $user->user_email . $user->user_login . $user->user_registered . $user->user_pass. wp_create_nonce($action);
    }
    
    public static function verify_token($token, $action = 'api'){
        // token format is {user_id}|{ email + login + registered date + password hash}
        
        $data = explode ('|', $token);
        
        if(count($data) !== 2){
            return false;
        }
        
        $user_id = $data[0];
        $recieved_token = $data[1];
        
        $user =  get_user_by('ID', $user_id);
        
        if( password_verify(self::get_token_string($user, $action), $recieved_token) === false){
            return false;
        }
       
        return $user_id;
    }
   
    public static function verify_nonce($token,$action = 'api'){       
       $user_id = self::verify_token($token);
       if($user_id){
        return Scolaa_Api_Helper::get_success_response('','Current User Details', get_userdata($user_id));
       }
       return Scolaa_Api_Helper::get_error_response('invalid', 'Unauthorized User', false);
   }
    
   public static function register_routes() {
       $namespace = 'app/v1';
        register_rest_route( 
            $namespace, 
            '/login/', 
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_called_class(), 'login_to_app'),
                'args' => array(
                    'user' => array(
                        'required' => true,
                        //'default' => 'value',
                        //'sanitize_callback' => 'absint', 
                        //'validate_callback' =>  function($param, $request, $key) {
        				//	return is_numeric( $param );
        				//}
                    ),
                    'password' => array(
                        'required' => true,
                        //'default' => 'value',
                        //'sanitize_callback' => 'absint', 
                        //'validate_callback' =>  function($param, $request, $key) {
        				//	return is_numeric( $param );
        				//}
                    ),
                ),
                //'permission_callback' => function () {
        		//	return current_user_can( 'edit_others_posts' );
        		//}
            ) 
        );
        
        register_rest_route( 
            $namespace, 
            '/check-login/', 
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_called_class(), 'check_login'),
                
                'permission_callback' => function () {
        			return is_user_logged_in();
        		}
            ) 
        );
            
        register_rest_route( 
            $namespace, 
            '/forget-password/', 
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_called_class(), 'forget_password'),
                'args' => array(
                    'user_login' => array(
                        'required' => true,
                        //'default' => 'value',
                        //'sanitize_callback' => 'absint', 
                        //'validate_callback' =>  function($param, $request, $key) {
        				//	return is_numeric( $param );
        				//}
                    )
                ),
                //'permission_callback' => function () {
        		//	return current_user_can( 'edit_others_posts' );
        		//}
            ) 
        );
        
        register_rest_route( 
            $namespace, 
            '/verify/nonce', 
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array(get_called_class(), 'verify_nonce'),
                'args' => array(
                    '_wpnonce' => array(
                        'required' => true                        
                    ),
                    'token' => array(
                        'required' => true
                    )
                ),
                'permission_callback' => function () {
                        return is_user_logged_in();
                }
            ) 
        );
   }
   
   
   
   public static function login_to_app( $request){ // object of WP_REST_Request
        global $current_user;
        $params = $request->get_params();
        
        $user = get_user_by( 'login', $params['user'] );
        if ( $user && wp_check_password(  $params['password'], $user->data->user_pass, $user->ID) ){
            
            $token = self::generate_token($user);
           
            return new WP_REST_Response( array('status' => true, 'token' => $token), 200 ); 
        }
        return new WP_REST_Response( array('status' => false, 'message' => 'user name or password mismatch'), 403 );
   }
   
   public static function forget_password($data){
       global $valtra_sms;
       $user = get_user_by( 'login', $data['user_login'] );
       $user_phone_number = get_user_meta($user->ID,'celular',true);
       
       $user_login = $data['user_login'];
        if ( $user ){
            $user_email = $user->user_email;
        
            $token = self::generate_token($user);
            
            $message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
            $message .= network_home_url( '/adm' ) . "\r\n\r\n";
            $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
            $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
            $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
            $message .= '<a href=http://nightpass.scit.com.br/adm/index.html#/reset-password?token=' . $token . ">http://nightpass.scit.com.br/adm/index.html#/reset-password?token=" . $token ."</a>\r\n";
            
            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

            $title = sprintf( __('[%s] Password Reset'), $blogname );

            $title = apply_filters('retrieve_password_title', $title);
            if ( $message && !wp_mail($user_email, $title, $message) )
                 return new WP_REST_Response( array('status' => false, 'message' => 'Possible reason: your host may have disabled the mail() function...'), 403 );   
            
            $sms_result =  $valtra_sms->send_sms($user_phone_number,'testing');
            return new WP_REST_Response( array('status' => true, 'message' => 'success',$sms_result), 200 ); 
        }
        return new WP_REST_Response( array('status' => false, 'message' => 'User Not Found'), 403 );
   }
   
   public static function check_login(){
       global $current_user;
      
       return new WP_REST_Response( $current_user , 200 ); 
   }
}
Scolaa_Api_Auth::start();