<?php
class Scolaa_Api_V1{

    /**
     * Load all starting libraries and action for this module
     */
    public static function start() {
        self::init();    
    }  
    
    public static function init(){
        global $CONFIG;
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-auth");
      remove_filter('rest_pre_serve_request', 'rest_send_cors_headers');
      add_filter('rest_pre_serve_request', array(get_called_class(), 'add_cors_header'), 15);
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-helper");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-hospitals");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-language");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-domains");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-domain-translate");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-feedback-questions");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-feedback-options");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-op-domains");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-op-feedback-questions");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-op-feedback-options");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-op-dashboard");
      
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-ip-domains");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-ip-feedback-questions");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-ip-feedback-options");
      Scolaa_Modules::load_class("scolaa-api","class-scolaa-api-ip-dashboard");
    } 
    
    public static function add_cors_header($value) {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Headers: accept, content-type, scolaa-authorization');

        return $value;
    }
}
Scolaa_Api_V1::start();
