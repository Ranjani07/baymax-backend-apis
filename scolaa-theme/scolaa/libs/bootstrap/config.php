<?php
/**
 * register all client library like bootstrap,jquery
 * 
 */ 
Scolaa_Config::register_library(
    'bootstrap',
    array(
        'url' => '',
        'version' => '3.3.6',
        'added_on' => 'some date',
        'folder' => '<folder name>',
        'dev' => array(
            'script' => array(
                'bootstrap-js' => array(
                    'filepath' => '/js/bootstrap.js',
                    'version'  => '3.3.6',
                    'dependancy' => array(),
                    'async' => true
                )
            ),
            'style' => array(
                'bootstrap-css' => array(
                    'filepath' => '/css/bootstrap.css',
                    'version'  => '3.3.6',
                    'dependancy' => array()
                ),
                'bootstrap-theme-css' => array(
                    'filepath' => '/css/bootstrap-theme.css',
                    'version'  => '3.3.6',
                    'dependancy' => array()
                ),
            )
        ),
        'production' => array(
             'script' => array(
                'bootstrap-min-js' => array(
                    'filepath' => '/js/bootstrap.min.js',
                    'version'  => '3.3.6',
                    'dependancy' => array(),
                    'async' => true
                )
            ),
            'style' => array(
                'bootstrap-min-css' => array(
                    'filepath' => '/css/bootstrap.min.css',
                    'version'  => '3.3.6',
                    'dependancy' => array()
                ),
                'bootstrap-theme-min-css' => array(
                    'filepath' => '/css/bootstrap-theme.min.css',
                    'version'  => '3.3.6',
                    'dependancy' => array()
                ),
            )
        ),
    )
);