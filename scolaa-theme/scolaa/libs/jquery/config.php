<?php

Scolaa_Config::register_library(
    'jquery',
    array(
        'url' => '',
        'version' => '',
        'added_on' => 'some date',
        'folder' => '<folder name>',
        'dev' => array(
            'script' => array(
                'jquery-js' => array(
                    'filepath' => '/jquery-1.12.4.js',
                    'version'  => '1.12.4',
                    'dependancy' => array(),
                    'async' => true
                )
            )
        ),
        'production' => array(
             'script' => array(
                'jquery-min-js' => array(
                    'filepath' => '/jquery-1.12.4.min.js',
                    'version'  => '1.12.4',
                    'dependancy' => array(),
                    'async' => true
                )
            )
        ),
    )
);

