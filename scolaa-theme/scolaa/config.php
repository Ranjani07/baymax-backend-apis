<?php

Scolaa_Config::register_app(
    array(
        'display_name' => "Some app name",
        'description' => 'some description',
        'git' => 'bitbucket url',
        'prefix' => 'scolaa',
        'version' => '1.0.0',
        'language' => 'en',
        'environments' => array(
            'dev' => array(
                'url' => '',
                'debug_mode' => 'true'
            ),
            'production' => array(
                'url' => '',
                'debug_mode' => 'false',
            ),
        ),
        'app_url' => get_template_directory_uri() .'/scolaa',
        'app_dir' => __DIR__,
        'active_environment' => 'dev',
    )
);

