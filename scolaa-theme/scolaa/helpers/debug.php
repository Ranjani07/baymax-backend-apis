<?php

class Scolaa_Debug{
    
    public static $debug_mode;
    
    public static function start(){
        self::$debug_mode = Scolaa_App::get_active_environment()['debug_mode'];
    }

    public static function print_object($object, $should_i_die = false){
        if(self::$debug_mode === false){
            return;
        }
        echo '<pre>';
        print_r($object);
        echo '</pre>';
        if($should_i_die === true){
            die('<br/><br/>Stopping Execution after printing the object.....');
        }
    }
    
    public static function dump_object($object, $should_i_die = false){
        if(self::$debug_mode === false){
            return;
        }
    
        echo '<pre>';
        var_dump($object);
        echo '</pre>';
        if($should_i_die === true){
            die('<br/><br/>Stopping Execution after dumpping the object.....');
        }
    }
    
    public static function stack_trace(){
    
        if(self::$debug_mode === false){
            return;
        }
        
        echo '<pre>';
        print_r(debug_backtrace());
        echo '</pre>';
        die('<br/><br/>Stopping Execution after printing the object.....');
    }
    
    public static function log($message, $object){
        if(self::$debug_mode === false){
            return;
        }
        
        /**
         * http://php.net/manual/en/function.error-log.php
         * 
         */
    }
    
    public static function force_log($message, $object){
        
    }
    
    
}

Scolaa_Debug::start();