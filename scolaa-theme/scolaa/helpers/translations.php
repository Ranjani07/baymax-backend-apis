<?php

/**
 *  global functions for Translations
 */
class Scolaa_Translations {
    
    public static function start() {
       
    }
    
    public static function get_language($language){
        global $CONFIG;
        return isset($CONFIG["translations"][$language])? $CONFIG["translations"][$language]: false;
    }
    
    public static function get_translation_scope($language, $scope){
        $language = self::get_language($language);
        return isset($language[$scope]) ? $language[$scope] : false;
    }
    
    public static function get($tranlation_key, $scope, $language = ''){
        if($language === ""){
            $language = Scolaa_App::get_language();
        }
        
        $translations = self::get_translation_scope($language, $scope);
        
        $translation = isset($translations[$tranlation_key]) ? $translations[$tranlation_key] : false;
        
        if($translation === false){
            return "Translation string no found";
        }
        
        return translation;
        
    }
    public static function _print($tranlation_key, $scope, $language = ''){
        echo self::get($tranlation_key, $scope, $language);
        
    }
    
    
}
Scolaa_Translations::start();