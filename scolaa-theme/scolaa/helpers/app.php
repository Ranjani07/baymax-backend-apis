<?php

/**
 *  global functions for App
 */
class Scolaa_App {
    
    public static function start() {
       
    }
    
    public static function get_display_name(){
        global $CONFIG;
        return $CONFIG['app']['display_name'];
    }
    
    public static function get_description(){
        global $CONFIG;
        return $CONFIG['app']['description'];
    }
    
    public static function get_environments(){
        global $CONFIG;
        return $CONFIG['app']['environments'];
    }
    public static function get_active_environment_name(){
        global $CONFIG;
        return $CONFIG['app']['active_environment'];
    }
    public static function get_active_environment(){
        global $CONFIG;
        $environmennts = self::get_environments();
        $active_enviroment = self::get_active_environment_name();
        return isset($environmennts[$active_enviroment]) ? $environmennts[$active_enviroment] : false;
    }
    
    public static function get_git(){
        global $CONFIG;
        return $CONFIG['app']['git'];
    }
    
     public static function get_prefix(){
        global $CONFIG;
        return $CONFIG['app']['prefix'];
    }
    
     public static function get_version(){
        global $CONFIG;
        return $CONFIG['app']['version'];
    }
    
    public static function get_language(){
        global $CONFIG;
        return $CONFIG['app']['language'];
    }
    
    public static function get_app_url(){
        global $CONFIG;
        return $CONFIG['app']['app_url'];
    }
    public static function get_app_dir(){
        global $CONFIG;
        return $CONFIG['app']['app_dir'];
    }
    
}
Scolaa_App::start();