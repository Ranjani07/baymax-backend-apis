<?php

/**
 *  global functions for modules
 */
class Scolaa_Modules {

    /**
     * Load all starting module and action for this module
     */
    public static function start() {
        
    }
    
    public static function init(){
        
    }
    
    public static function get_version($module_name){
        global $CONFIG;
        return "v".$CONFIG['modules'][$module_name]['version'];
    }
    
    public static function is_module_enabled($module_name){
        global $CONFIG;
        return $CONFIG['modules'][$module_name]['enabled'];
    }
    
    public static function is_module_class_enabled($module_name, $class_key){
        global $CONFIG;
        return $CONFIG['modules'][$module_name]['classes'][$class_key]['enabled'];
    }
    
    public static function get_modules(){
        global $CONFIG;
        return $CONFIG['modules']; 
    }
    
    public static function get($module_name){
        global $CONFIG;
        $modules = self::get_modules();
        return isset($modules[$module_name]) ? $modules[$module_name] : false; 
    }
    
    public static function load($module_name){
        $version = self::get_version($module_name);
        if(self::is_module_enabled($module_name)){
            require_once self::get_module_path($module_name) . "{$module_name}-{$version}.php";
        }
    }
    
    public static function get_module_path($module_name){
        global $CONFIG;
        $version = self::get_version($module_name);
        return Scolaa_App::get_app_dir() . "/modules/{$module_name}-{$version}/";
    }
    
    public static function get_classes_path($module_name){
        global $CONFIG;
        return self::get_module_path($module_name). 'classes/';
    }
    
    public static function get_class($module_name, $class_key){
        global $CONFIG;
        $module = self::get($module_name);
        return isset($module['classes'][$class_key]) ? $module['classes'][$class_key] : false;
    }
    
    public static function load_class($module_name, $class_key){
        global $CONFIG;
        /**
         * check enable/disable module here
         */ 
         if(self::is_module_class_enabled($module_name,$class_key)){
            require_once self::get_classes_path($module_name) . self::get_class($module_name, $class_key)['filepath'];
         }
    }
}
Scolaa_Modules::start();