<?php

class Scolaa_Config {

    /**
     * Load all starting module and action for this module
     */
    public static function start() {

    }

    /**
     * Load global theme config
     */
    public static function register_app($data) {
        global $CONFIG;
        $CONFIG['app'] = $data;
    }

    /**
     * Load global template config
     */
    public static function register_template($scope, $template_name, $data) {
        if(empty($data)){
            return;
        }
        global $CONFIG;
        $CONFIG["templates"][$scope][$template_name] = $data;
    }
    
    /**
     * Load global translation config
     */
    public static function register_translation($lang = "en", $scope = "translations", $data) {
        if(empty($data)){
            return;
        }
        global $CONFIG;
        $CONFIG["translations"][$lang][$scope] = $data;
    }
    
    /**
     * Load module config
     */
    public static function register_module($module_name, $data) {
        if(empty($data)){
            return;
        }
        global $CONFIG;
        
        $module_name = "{$CONFIG['app']['prefix']}-{$module_name}";
        $CONFIG['modules'][$module_name] = $data;
    }
    
    /**
     * Load global library config
     */
    public static function register_library( $lib_name, $data) {
        if(empty($data)){
            return;
        }
        global $CONFIG;
        $CONFIG["libs"][$lib_name] = $data;
    }
    
     /**
     * Load global assets/img config
     */
    public static function register_image( $scope, $path,  $image_file_key="") {
         
        if(empty($scope) || empty($path) || empty($image_file_key)){
            return;
        }
       
        global $CONFIG;
        $CONFIG["assets"]["img"][$scope][$image_file_key] = $path;
    }
     
    /**
     * Get All config files
     */
    public static function get_all_config() {
        $config_files = self::array_flatten(self::convert_directory_to_array(Scolaa_App::get_app_dir(), "config.php"));
        return $config_files;
    }
    
    public static function convert_directory_to_array($dir,$filter = "",$append_name="") { 
   
        $result = array(); 
        $all_files = array();
        $cdir = scandir($dir);
        
        $img_directory_path = Scolaa_Assets::get_image_directory_path();
        if(!empty($filter)){
            foreach ($cdir as $key => $value) 
            {               
                $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
                    if (!in_array($value,array(".",".."))) 
                    { 
                        if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
                        { 
                             $all_files[] = self::convert_directory_to_array($dir . DIRECTORY_SEPARATOR . $value, $filter); 
					        
                         } 
                         else 
                         { 
                             
                             if($value === "config.php"){
                                $all_files[] = $path;
                             }
                             
                         } 
                    } 
                
           
            }
            return $all_files; 
        }
        else{
            foreach ($cdir as $key => $value) 
            {                
                $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
                  if (!in_array($value,array(".",".."))) 
                  { 
                     if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
                     {       
                        $result[$value] = self::convert_directory_to_array($dir . DIRECTORY_SEPARATOR . $value, $filter,$append_name); 
                     } 
                     else 
                     { 
                        $basename = str_replace($img_directory_path,"",$path);
                        $result[$basename] = $path;                       
                     } 
                  } 
            }
            return $result; 
        }
       
    } 
    
    public static function array_flatten($array) { 
      if (!is_array($array)) { 
        return FALSE; 
      } 
      $result = array(); 
      foreach ($array as $key => $value) { 
        if (is_array($value)) { 
          $result = array_merge($result, self::array_flatten($value)); 
        } 
        else { 
          $result[$key] = $value; 
        } 
      } 
      return array_filter($result); 
    } 
}
Scolaa_Config::start();