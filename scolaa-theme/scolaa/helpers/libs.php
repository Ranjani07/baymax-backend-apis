<?php

/**
 *  global functions for Libs
 */
class Scolaa_Libs {

    /**
     * Load all starting libraries and action for this library
     */
    public static function start() {
        add_filter('script_loader_tag', array(get_called_class(), 'add_defer_attribute'), 10, 2);
    }
    
    public static function init(){
        
    }
    
    public static function add_defer_attribute($tag, $handle) {
        if (strpos($handle, 'async') === 0)
            return str_replace(' src', ' async="async" src', $tag);
        return $tag;
    }
    
    public static function get($library_name){
        global $CONFIG;
        $environment = Scolaa_App::get_active_environment_name();
        return $CONFIG['libs'][$library_name][$environment];
    }
    
    public static function load($library_name){
        $libaries = self::get($library_name);
        foreach($libaries as $key => $library){
            
            foreach($library as $name => $values){
                if($key === "script"){
                    self::load_script($library_name,$name);
                }
                elseif($key === "style"){
                    self::load_style($library_name,$name);
                }
            }
        }
        
    }
    
    
    public static function get_library_path(){
        return Scolaa_App::get_app_dir() . '/libs/';
    }
    
    public static function get_library_url(){
        return Scolaa_App::get_app_url() . "/libs/";
    }
    
    public static function get_script_url($library_name, $script_name){
        $library_url = self::get_library_url();
        $script = self::get_script($library_name, $script_name);
        return $library_url.$library_name.$script['filepath'];
    }
    
    public static function get_script_path($library_name, $script_name){
        $library_path = self::get_library_path();
        $script = self::get_script($library_name, $script_name);
        return $library_path.$library_name.$script['filepath'];
    }
    
    public static function get_style_url($library_name, $style_name){
        $library_url = self::get_library_url();
        $style = self::get_style($library_name, $style_name);
        
        return $library_url.$library_name. $style['filepath'];
    }
    
    
    public static function get_style_path($library_name, $style_name){
        $library_path = self::get_library_path();
        $style = self::get_style($library_name, $style_name);
        
        return $library_path.$library_name.$style['filepath'];
    }
    
    public static function get_script($library_name, $script_name){
        global $CONFIG;
        
        $environment = Scolaa_App::get_active_environment_name();
        return $CONFIG['libs'][$library_name][$environment]['script'][$script_name];
    }
    
    public static function get_style($library_name, $style_name){
        global $CONFIG;
        $environment = Scolaa_App::get_active_environment_name();
        return $CONFIG['libs'][$library_name][$environment]['style'][$style_name];
    }
    
    public static function load_style($library_name, $style_name) {
        
        $style = self::get_style($library_name, $style_name);
        
        if(!file_exists(self::get_style_path($library_name, $style_name))) {
            return false;
        }
        
        wp_enqueue_style(
            "lib-{$style_name}", 
            self::get_style_url($library_name, $style_name),
            $style["dependancy"], 
            $style["version"]
        );

        
        
    }

    public static function load_script($library_name,$script_name) {
        $script = self::get_script($library_name, $script_name);
        
         if(!file_exists(self::get_script_path($library_name, $script_name))){
            return false;
        }
        
        if(isset($script["async"]) && $script["async"] === true){
            $script_alias_name = "async-lib-{$script_name}";
        }

        wp_enqueue_script(
            $script_alias_name, 
            self::get_script_url($library_name, $script_name),
            $script["dependancy"], 
            $script["version"]
        );
    }
    
     
}
Scolaa_Libs::start();