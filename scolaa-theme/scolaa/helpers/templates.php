<?php

/**
 *  global functions for templates
 */
class Scolaa_Templates {
    /**
     * Load all starting template and action for this template
     */
    public static function start() {
        
    }
    
    public static function init(){
        
    }
    
    public static function get_version($scope,$template_name){
        global $CONFIG;        
        return "v".$CONFIG["templates"][$scope][$template_name]['version'];
    }
    
    public static function process($template_name){
        
    }

    public static function get_templates(){
        global $CONFIG;
        return $CONFIG['templates']; 
    }
    
    
    public static function get_template($scope, $template_key){
        global $CONFIG;        
        $template = self::get_templates();
        return isset($template[$scope][$template_key]) ? $template[$scope][$template_key] : false; 
    }
    
    public static function get_template_path($scope, $template_key){
        global $CONFIG;
        $template = self::get_template($scope, $template_key);
        $version = self::get_version($scope,$template_key);
        return Scolaa_App::get_app_dir() . "/templates/{$CONFIG['app']['prefix']}-{$scope}-{$version}/{$template['filepath']}";
    }
    
    public static function get($scope, $template_key, $data = array()){
        ob_start();
        require_once self::get_template_path($scope, $template_key);
        return ob_get_clean();
    }
    
    public static function load($scope, $template_key, $data = array()){        
        require_once self::get_template_path($scope, $template_key);
    }
   
}