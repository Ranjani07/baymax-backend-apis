<?php

/**
 *  global functions for Assets
 */
class Scolaa_Assets {
    
    
    public static function start() {
    }
    
    public static function init(){
        
        wp_enqueue_style('scolaa-app', self::get_style_url() . 'app.css');
        wp_enqueue_script('scolaa-app', self::get_script_url() . 'app.js', array( 'jquery' ), '1.0', true);
    }
    
    public static function add_defer_attribute($tag, $handle) {
        if (strpos($handle, 'async') === 0)
            return str_replace(' src', ' async="async" src', $tag);
        return $tag;
    }
   
    public static function get_assets_path(){
        return Scolaa_App::get_app_dir() . '/assets/';
    }
    
    public static function get_assets_url(){
        return Scolaa_App::get_app_url() . "/assets/";
    }
    
    public static function get_script_url($script_name){
        return self::get_assets_url() . "/js/{$script_name}";
    }

    public static function get_style_url($style_name){
        return self::get_assets_url(). "/css/{$style_name}";
    }
    
    public static function get_image_directory_path(){
        return self::get_assets_path() . 'img/';
    }
    
    public static function get_image_directory_url(){
        return self::get_assets_url() . 'img/';
    }
    
    public static function get_image_url($name, $scope){
        
        $image_url =  self::get_image_directory_url() . "{$scope}/{$name}";
        
        $image_path = self::get_image_directory_path() . "{$scope}/{$name}";
        
        if(!file_exists($image_path)){
            return self::get_image_directory_path() . 'default.jpg';
        }
        return $image_url;
    }
   
    public static function register_images(){
       $all_files = Scolaa_Config::array_flatten(Scolaa_Config::convert_directory_to_array(self::get_image_directory_path()));
       self::get_images_recursive($all_files);
    }
    
    public static function get_images_recursive($image_files){
        //Scolaa_Debug::print_object($image_files,true);
        foreach($image_files as $image_key => $image_file_path){ 
            $scope = explode('/',$image_key)[0];       
            $file_name = basename($image_file_path);
            Scolaa_Config::register_image($scope,$image_file_path, $image_key);
        
       }
    }
    
    public static function load_localize_script($library_name,$script_name,$dependancies = array(), $object_name, $data,$version='1.0.0.1') {
        $script = Scolaa_Libs::get_script($library_name, $script_name);
        
         if(!file_exists(Scolaa_Libs::get_script_path($library_name, $script_name))){
            return false;
        }
        
        if(isset($script["async"]) && $script["async"] === true){
            $script_alias_name = "async-lib-{$script_name}";
        }
        
        wp_localize_script($script_alias_name, $object_name, $data);
        wp_enqueue_script(
            $script_alias_name, 
            Scolaa_Libs::get_script_url($library_name, $script_name),
           $dependancies, 
            $version
        );


        
    }
    
    
}
Scolaa_Assets::start();