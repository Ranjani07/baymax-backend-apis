<?php

require_once(__DIR__ . '/helpers/configuration.php');
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/helpers/app.php';
require_once __DIR__ . '/helpers/debug.php';
require_once __DIR__ . '/helpers/translations.php';
require_once __DIR__ . '/helpers/assets.php';
require_once __DIR__ . '/helpers/libs.php';
require_once __DIR__ . '/helpers/modules.php';
require_once __DIR__ . '/helpers/templates.php';



/**
 * Intialize everything here
 * 
 */ 
$config_files = Scolaa_Config::get_all_config();
foreach ($config_files as $key => $config_file) {
    require_once($config_file);
}


Scolaa_Assets::register_images();
Scolaa_Modules::load("scolaa-theme");
Scolaa_Modules::load("scolaa-feedback");
Scolaa_Modules::load("scolaa-hospitals");
Scolaa_Modules::load("scolaa-api");
Scolaa_Modules::load("scolaa-users");
Scolaa_Modules::load("scolaa-phpexcel");
global $CONFIG;


// $user = [];
// Scolaa_Modules::load_class("scolaa-participate","class-i-want-participate");
// $user = Scolaa_Want_Participant::insert_reseller_data($user);
// echo $user;
