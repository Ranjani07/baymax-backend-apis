<?php

$lang = 'pt-br';
Scolaa_Config::register_translation(
    $lang, 
    'global', 
    array(
        'company_name' => '',
    )
);

Scolaa_Config::register_translation(
    $lang, 
    'scolaa-core', 
    array(
        'company_name' => '',
    )
);

Scolaa_Config::register_translation(
    $lang, 
    'scolaa-theme', 
    array(
        'company_name' => '',
    )
);

Scolaa_Config::register_translation(
    $lang, 
    'scolaa-debug', 
    array(
        'company_name' => '',
    )
);
