<?php

Scolaa_Config::register_template(
    'theme', 
    'promotion_page_template',
    array(
        'name' => '',
        'description' => '',
        'version' => '1',
        'filepath' => 'promotion/page-promotion-template.php',
        'data' => array(
            'parameter_1' => array(
                'type' => '',
                'required' => true,
                'description' => '',
                'notes' => '',
            ),
            'parameter_2' => array(
                'type' => '',
                'required' => true,
                'description' => '',
                'notes' => '',
            ),
        )
    )
);

Scolaa_Config::register_template(
    'theme',
    'page-shell-video-template',
    array(
        'name' => '',
        'description' => '',
        'version' => '1',
        'filepath' => 'shell-videos/page-shell-video-template.php',
        'data' => array(
            'parameter_1' => array(
                'type' => '',
                'required' => true,
                'description' => '',
                'notes' => '',
            ),
            'parameter_2' => array(
                'type' => '',
                'required' => true,
                'description' => '',
                'notes' => '',
            ),
        )
    )
);