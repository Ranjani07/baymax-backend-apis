<?php



Scolaa_Config::register_template(
    'core', 
    'template_name_1',
    array(
        'name' => '',
        'description' => '',
        'version' => '1-0-0',
        'filepath' => 'relative file path from the current template folder with slash',
        'data' => array(
            'parameter_1' => array(
                'type' => '',
                'required' => true,
                'description' => '',
                'notes' => '',
            ),
            'parameter_2' => array(
                'type' => '',
                'required' => true,
                'description' => '',
                'notes' => '',
            ),
        )
    )
);

Scolaa_Config::register_template(
    'core',
    'template_name_2',
    array(
        'name' => '',
        'description' => '',
        'version' => '1-0-0',
        'filepath' => 'relative file path from the current template folder with slash',
        'data' => array(
            'parameter_1' => array(
                'type' => '',
                'required' => true,
                'description' => '',
                'notes' => '',
            ),
            'parameter_2' => array(
                'type' => '',
                'required' => true,
                'description' => '',
                'notes' => '',
            ),
        )
    )
);